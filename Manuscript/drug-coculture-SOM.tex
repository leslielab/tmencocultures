\documentclass[english]{article}
\usepackage[T1]{fontenc}
\usepackage[latin9]{inputenc}
\usepackage[letterpaper]{geometry}
\geometry{verbose,tmargin=1in,bmargin=1in,lmargin=1in,rmargin=1in}
\usepackage{amstext}
\usepackage[authoryear]{natbib}
\usepackage{url,amsmath}
\usepackage{multirow}
\usepackage{graphicx}
\usepackage[labelfont=bf]{caption}
\usepackage{color}
\usepackage{soul}
\usepackage{float}
\restylefloat{table}

\usepackage{subcaption}

\newcommand{\beginsupplement}{%
        \setcounter{figure}{0}
        \renewcommand{\thefigure}{S\arabic{figure}}%
     }

\beginsupplement

\makeatletter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Textclass specific LaTeX commands.
\newcommand{\lyxaddress}[1]{
\par {\raggedright #1
\vspace{1.4em}
\noindent\par}
}
\newenvironment{lyxlist}[1]
{\begin{list}{}
{\settowidth{\labelwidth}{#1}
 \setlength{\leftmargin}{\labelwidth}
 \addtolength{\leftmargin}{\labelsep}
 \renewcommand{\makelabel}[1]{##1\hfil}}}
{\end{list}}

\makeatother

\usepackage{babel}
\newcommand{\TODO}[1]{{\bf [[TODO:  #1]]}}

\newcommand{\kld}{$D_\text{KL}$}
\newcommand{\logkld}{$\log(D_\text{KL})$}

\begin{document}

\title{Modeling tumor-stromal interactions that mediate innate
resistance to cancer therapies:\\
Supplementary note}

\author{Julie L. Yang$^{\text{}1,2}$, Robert L. Bowman$^{\text{} 3}$,  Raphael Pelossof$^{\text{}1}$, Johanna A. Joyce$^{\text{} 3}$,\\  Christina S. Leslie$^{\text{}1,*}$}

\maketitle

\lyxaddress{1. Computational Biology Program, Memorial Sloan Kettering Cancer Center, New York, NY.\\
2. Tri-I Program in Computational Biomedicine, Weill Cornell Graduate College, New York, NY.\\
3. Cancer Biology and Genetics Program, Memorial Sloan Kettering Cancer Center, New York, NY.}


\lyxaddress{{*} Corresponding Author: Christina Leslie, \url{cleslie@cbio.mskcc.org}.}

\newpage{}

\date{}
\tableofcontents
\listoffigures
%\listoftables
%\listoftables
\newpage{}
\section{Supplementary Figures}


\begin{figure}[htp!]
\centering
 \includegraphics[width=1\textwidth]{../Figures/SOM/cytokines.pdf}
    \caption[{\bf Cytokine expression and correlation}]{{\bf Cytokine expression and correlation.} (a) The heatmap of normalized cytokine expression levels shows patterns of expression of 44 cytokines passing a threshold of minimum maximum expression level of 0.75 across 18 stromal cell lines. (b) The cytokines are not highly correlated with each other. HGF is correlated with MMP-10, LAP, and VEGF-C. SDF-1 is correlated with TNF-beta, FGF-6, and IL-3. There is a cluster of correlated cytokines including: IL-1 alpha, GM-CSF, GRO-alpha, IL-1 beta, MIP-3 alpha, MIP-1 alpha, bFGF, GCSF, MCP-3, GRO, Cathepsin S, and uPAR.}
\label{fig:cytokine:heatmaps}
\end{figure}


\begin{figure}
\centering
 \includegraphics[width=1\textwidth]{../Figures/SOM/pathways.pdf}
    \caption[{\bf Pathway correlation}]{{\bf Pathway correlation.} The pathways are also not highly correlated with each other. The MET pathway is correlated with PDGFRB pathway, IL3 pathway, ERBB4 pathway, MTOR pathway, PI3KCIAKT pathway, NFAT pathway. The MYC pathway is not highly correlated with any of the other pathways.}
\label{fig:pathway:heatmaps}
\end{figure}

\begin{figure}
\centering
 \includegraphics[width=1\textwidth]{../Figures/SOM/ARMTLPathwaysvsReceptors.pdf}
    \caption[{\bf Multi-task affinity regression model with pathway scores vs.\ model with receptors and signaling components performance}]{{\bf Multi-task affinity regression model with pathway scores vs.\ model with receptors and signaling components performance.} Multi-task affinity regression models with pathway scores outperform models with receptors and signaling components in 10-fold cross-validation experiments on co-cultures treated with BRAF, MEK, EGFR inhibitors and docetaxel.  Multi-task affinity regression with pathway scores outperformed models with cancer input summarized as receptors and signaling components (P $\textless$ 1.06 $\times$ 10-2, Wilcoxon signed rank test.)}
\label{fig:performance:receptors}
\end{figure}


\begin{figure}
\centering
 \includegraphics[width=1\textwidth]{../Figures/SOM/ARMTLvsLeastRperformance.pdf}
    \caption[{\bf Multi-task vs.\ regular least squares affinity regression performance}]{{\bf Multi-task vs.\ regular least squares affinity regression performance.} Multi-task affinity regression outperforms independent linear regressions in 10-fold cross-validation experiments.  Multi-task affinity regression strongly outperformed independent linear regressions across individual tasks (P $\textless$ 8.72 $\times$ 10-8, Wilcoxon signed rank test.)}
\label{fig:performance:graphs}
\end{figure}


% Afatinib
\begin{figure}[htp!]
\centering
 \includegraphics[width=1\textwidth]{../Figures/SOM/Heatmaps/Afatinib.pdf}
    \caption[{\bf Afatinib pathway mapping and interaction scores}]{{\bf Afatinib pathway mapping and interaction scores.} (a) We created a heatmap of pathway-stromal mapping scores for afatinib treated co-cultures, computed one-sided p-values according to a null model similar to before. Orange boxes indicate pathway-stromal cell lines passing an FDR threshold of 5\% and dark orange boxes indicate pairs passing an FDR threshold of 10\% (b) We show the heatmap of the absolute value of interaction scores for afatinib treated co-cultures. We highlighted scores associated with either drug resistance or sensitivity that passed an FDR threshold of 5\% in orange and 10\% in dark orange. The HGF interaction with the PI3K/AKT pathway  passed our threshold of FDR 10\%.}
\label{fig:cytokinemappings:Afatinib:a}
\end{figure}



% EGFR

\begin{figure}
\centering
 \includegraphics[width=1\textwidth]{../Figures/SOM/Heatmaps/EGFR.pdf}
    \caption[{\bf EGFR head and neck cancer cell counts, pathway mapping, and interaction scores}]{{\bf EGFR  head and neck cancer cell counts, pathway mapping, and interaction scores.} (a) We counted the number of head and neck cancer cell lines with a significant interaction for a cytokine. This analysis suggests HGF is a cytokine mediating drug resistance in head and neck cancer. (b) We created a heatmap of pathway-stromal mapping scores for the pan-EGFR model, computed one-sided p-values according to a null model similar to before. The IL12 Stat 4 pathway, IL 12 2 pathway, CMYB pathway, and ALK1  and WNT non canonical pathway seem to be involved in mediating resistance in stromal cells. (c) We created a heatmap of interaction scores of the pan-EGFR inhibitor model. We highlighted scores associated with either drug resistance or sensitivity that passed an FDR threshold of 5\% in orange and 10\% in dark orange. HGF is a cytokine highly significant by an FDR threshold of 10\% in the interaction matrix.}
\label{fig:cytokinemappings:EGFR:a}
\end{figure}


% BRAF

\begin{figure}
\centering
 \includegraphics[width=1\textwidth]{../Figures/SOM/Heatmaps/BRAF.pdf}
    \caption[{\bf pan-BRAF/MEK inhibitor model cytokine and pathway mappings and interaction scores}]{{\bf pan-BRAF/MEK inhibitor model cytokine and pathway mappings and interaction scores.} (a) We created heatmaps of cytokine-cancer mapping scores for the pan-BRAF/MEK inhibitor model, computed one-sided p-values according to a null model similar to before, and we highlighted scores associated with either drug resistance or sensitivity that passed an FDR threshold of 5\% and 10\%. (b) We created heatmaps of pathway-stromal mapping scores for the pan-BRAF/MEK model, computed one-sided p-values according to a null model similar to before. The MYC pathway was associated with resistance in BRAF inhibitor treated co-cultures with skin, lung, and breast fibroblasts. (c) We show the heatmap of interaction scores for the pan-BRAF model. We highlighted scores associated with either drug resistance or sensitivity that passed an FDR threshold of 5\% in orange and 10\% in dark orange. }
\label{fig:cytokinemappings:BRAF:a}
\end{figure}



% PD184352

\begin{figure}
\centering
 \includegraphics[width=1\textwidth]{../Figures/SOM/Heatmaps/PD184352.pdf}
    \caption[{\bf PD184352 cytokine and pathway mappings and interaction scores}]{{\bf PD184352 cytokine and pathway mappings and interaction scores.} We created heatmaps of pathway-stromal mapping scores for PD184352 treated co-cultures, computed one-sided p-values according to a null model similar to before (Methods), and we highlighted scores associated with either drug resistance or sensitivity that passed an FDR threshold of 5\% and 10\%. HGF is a cytokine that was found to be associated with eliciting resistance in PD184352 treated co-cultures.}
\label{fig:cytokinemappings:PD184352:a}
\end{figure}




% SB590885

\begin{figure}
\centering
 \includegraphics[width=.975\textwidth]{../Figures/SOM/Heatmaps/SB590885.pdf}
    \caption[{\bf SB590885 cytokine and pathway mappings and interaction scores}]{{\bf SB590885 cytokine and pathway mappings and interaction scores.} (a) We created a heatmap of cytokine-cancer mapping scores for the model for co-cultures treated with SB590885, computed one-sided p-values according to a null model, and we highlighted scores associated with either drug resistance or sensitivity that passed an FDR threshold of 5\% and 10\%. Our model suggests HGF elicits resistance to SB590885 in melanoma. (b) We created a heatmap of pathway-stromal mapping scores for the SB590885 model, computed one-sided p-values according to a null model, and we highlighted scores associated with either drug resistance or sensitivity that passed an FDR threshold of 5\% and 10\%. The NF-$\kappa$B canonical pathway and the MYC pathway was associated with resistance in co-cultures with lung fibroblasts. The NFAT TF pathway was associated with resistance in co-cultures with stromal cells of all subtypes. (c) We created a heatmap of the interaction scores for the model of SB590885 treated co-cultures.}
\label{fig:cytokinemappings:SB590885:a}
\end{figure}


% AZD6244

\begin{figure}
\centering
 \includegraphics[width=1\textwidth]{../Figures/SOM/Heatmaps/AZD6244.pdf}
    \caption[{\bf AZD6244 cytokine and pathway mappings and interaction scores}]{{\bf AZD6244 cytokine and pathway mappings and interaction scores.} (a) We created a heatmap of cytokine-cancer mapping scores for the model for co-cultures treated with AZD6244, computed one-sided p-values according to a null model, and we highlighted scores associated with either drug resistance or sensitivity that passed an FDR threshold of 5\% and 10\%. This analysis suggests HGF elicits resistance to AZD6244 in melanoma. (b) We created a heatmap of pathway-stromal mapping scores for the AZD6244 model, computed one-sided p-values according to a null model, and we highlighted scores associated with either drug resistance or sensitivity that passed an FDR threshold of 5\% and 10\%. (c) We created a heatmap of the interaction scores for the model of AZD6244 treated co-cultures.}
\label{fig:cytokinemappings:AZD6244:a}
\end{figure}








% Erlotinib


\begin{figure}
\centering
 \includegraphics[width=0.97\textwidth]{../Figures/SOM/Heatmaps/Erlotinib.pdf}
    \caption[{\bf Erlotinib cytokine and pathway mappings and interaction scores}]{{\bf Erlotinib cytokine and pathway mappings and interaction scores.} (a) We created a heatmap of cytokine-cancer mapping scores for the model for co-cultures treated with erlotinib, computed one-sided p-values according to a null model, and we highlighted scores associated with either drug resistance or sensitivity that passed an FDR threshold of 5\% and 10\%. HFG is a cytokine associated with eliciting resistance in lung cancer cell lines treated with erlotinib. (b) We created a heatmap of pathway-stromal mapping scores for the erlotinib model, computed one-sided p-values according to a null model, and we highlighted scores associated with either drug resistance or sensitivity that passed an FDR threshold of 5\% and 10\%. (c) We created a heatmap of interaction scores for the model of erlotinib treated co-cultures. Interactions with the cytokine HGF and pathways including the ERBB2 ERBB3 pathway and the PI3K pathway passed thresholds of FDR 5\% and FDR 10\%.}
\label{fig:cytokinemappings:Erlotinib:a}
\end{figure}



% Canertinib

\begin{figure}
\centering
 \includegraphics[width=1\textwidth]{../Figures/SOM/Heatmaps/Canertinib.pdf}
    \caption[{\bf Canertinib cytokine and pathway mappings and interaction scores}]{{\bf Canertinib cytokine and pathway mappings and interaction scores.}  (a) We created a heatmap of cytokine-cancer mapping scores for the model for co-cultures treated with canertinib, computed one-sided p-values according to a null model, and we highlighted scores associated with either drug resistance or sensitivity that passed an FDR threshold of 5\% and 10\%. HGF is a cytokine that is predicted to elicit resistance in a lung cancer cell line HCC827.  (b) We created a heatmap of pathway-stromal mapping scores for the canertinib model, computed one-sided p-values according to a null model, and we highlighted scores associated with either drug resistance or sensitivity that passed an FDR threshold of 5\% and 10\%. (c) We created a heatmap of interaction scores for the model of canertinib treated co-cultures. Interactions with the cytokine HGF passed a threshold of FDR 10\%.}
\label{fig:cytokinemappings:Canertinib:a}
\end{figure}



% CL-387785
\begin{figure}
\centering
 \includegraphics[width=1\textwidth]{../Figures/SOM/Heatmaps/CL387785.pdf}
    \caption[{\bf CL-387785 cytokine and pathway mappings and interaction scores}]{{\bf CL-387785 cytokine and pathway mappings and interaction scores.} (a) We created heatmaps of cytokine-cancer mapping scores for the model for co-cultures treated with CL-387785, computed one-sided p-values according to a null model, and we highlighted scores associated with either drug resistance or sensitivity that passed an FDR threshold of 5\% and 10\%. HGF, DPPIV, and TNF-beta are cytokines found by the model to elicit resistance in CL-387785 treated co-cultures with lung fibroblasts. (b) We created a heatmap of pathway-stromal mapping scores for the CL-387785 model, computed one-sided p-values according to a null model, and we highlighted scores associated with either drug resistance or sensitivity that passed an FDR threshold of 5\% and 10\%. (c) We created a heatmap of interaction scores for the model of canertinib treated co-cultures. Interactions with cytokines IGFBP?6 and HGF passed an FDR threshold of 10\%.}
\label{fig:cytokinemappings:CL-387785:a}
\end{figure}

%Gefitinib
\begin{figure}
\centering
 \includegraphics[width=1\textwidth]{../Figures/SOM/Heatmaps/Gefitinib.pdf}
    \caption[{\bf Gefitinib cytokine and pathway mappings and interaction scores}]{{\bf Gefitinib cytokine and pathway mappings and interaction scores.} (a) We created a heatmap of cytokine-cancer mapping scores for the model for co-cultures treated with gefitinib, computed one-sided p-values according to a null model, and we highlighted scores associated with either drug resistance or sensitivity that passed an FDR threshold of 5\% and 10\%. HGF was found to elicit resistance in the lung cancer cell line HCC827 and in the head and neck cancer cell line SCC-15. (b) We created a heatmap of pathway-stromal mapping scores for the gefitinib model, computed one-sided p-values according to a null model, and we highlighted scores associated with either drug resistance or sensitivity that passed an FDR threshold of 5\% and 10\%. (c) We created a heatmap of interaction scores for the model of gefitinib treated co-cultures. No interactions passed the threshold of FDR 10\%.}
\label{fig:cytokinemappings:Gefitinib:a}
\end{figure}


%Lapatinib
\begin{figure}
\centering
 \includegraphics[width=1\textwidth]{../Figures/SOM/Heatmaps/Lapatinib.pdf}
    \caption[{\bf Lapatinib cytokine and pathway mappings and interaction scores}]{{\bf Lapatinib cytokine and pathway mappings and interaction scores.} (a) We created a heatmap of cytokine-cancer mapping scores for the model for co-cultures treated with lapatinib, computed one-sided p-values according to a null model, and we highlighted scores associated with either drug resistance or sensitivity that passed an FDR threshold of 5\% and 10\%. (b) We created a heatmap of pathway-stromal mapping scores for the lapatinib model, computed one-sided p-values according to a null model, and we highlighted scores associated with either drug resistance or sensitivity that passed an FDR threshold of 5\% and 10\%. (c) We created a heatmap of interaction scores for the model of lapatinib treated co-cultures. No interactions passed the threshold of FDR 10\%.}\label{fig:cytokinemappings:Lapatinib:a}
\end{figure}


%BCRABL
\begin{figure}
\centering
 \includegraphics[width=1\textwidth]{../Figures/SOM/Heatmaps/BCRABL.pdf}
    \caption[{\bf Dasatinib cytokine and pathway mappings and interaction scores}]{{\bf Dasatinib cytokine and pathway mappings and interaction scores.} (a) We created a heatmap of cytokine-cancer mapping scores for the model for co-cultures treated with dasatinib, computed one-sided p-values according to a null model, and we highlighted scores associated with either drug resistance or sensitivity that passed an FDR threshold of 5\% and 10\%. HGF was found to elicit resistance in the lung cancer cell line HCC4006, in the head and neck cancer cell line Cal27, and in the pancreatic cell line CFPAC-1.(b) We created a heatmap of pathway-stromal mapping scores for the dasatinib model, computed one-sided p-values according to a null model, and we highlighted scores associated with either drug resistance or sensitivity that passed an FDR threshold of 5\% and 10\%. (c) We created a heatmap of interaction scores for the model of dasatinib treated co-cultures. Interactions with the cytokine HGF passed a threshold of FDR 10\%.}
\label{fig:cytokinemappings:BCRABL:a}
\end{figure}

%VEGFR

\begin{figure}
\centering
 \includegraphics[width=1\textwidth]{../Figures/SOM/Heatmaps/VEGFR.pdf}
    \caption[{\bf Vandetanib cytokine and pathway mappings and interaction scores}]{{\bf Vandetanib cytokine and pathway mappings and interaction scores.} (a) We created a heatmap of cytokine-cancer mapping scores for the model for co-cultures treated with vandetanib, computed one-sided p-values according to a null model, and we highlighted scores associated with either drug resistance or sensitivity that passed an FDR threshold of 5\% and 10\%. HGF and DPPIV are cytokines predicted to elicit resistance in lung cancer cell lines. (b) We created a heatmap of pathway-stromal mapping scores for the vandetanib model, computed one-sided p-values according to a null model, and we highlighted scores associated with either drug resistance or sensitivity that passed an FDR threshold of 5\% and 10\%. (c) We created a heatmap of interaction scores for the model of vandetanib treated co-cultures. Interactions with HGF and DPPIV passed the threshold of FDR 10\%.}
\label{fig:cytokinemappings:Vandetanib:a}
\end{figure}


% Docetaxel

\begin{figure}
\centering
 \includegraphics[width=0.97\textwidth]{../Figures/SOM/Heatmaps/Docetaxel.pdf}
    \caption[{\bf Docetaxel cytokine and pathway mappings and interaction scores}]{{\bf Docetaxel cytokine and pathway mappings and interaction scores.} (a) We created a heatmap of cytokine-cancer mapping scores for the model for co-cultures treated with docetaxel, computed one-sided p-values according to a null model, and we highlighted scores associated with either drug resistance or sensitivity that passed an FDR threshold of 5\% and 10\%. Decorin is a cytokine predicted to elicit resistance in head and neck cancer and lung cancer cell lines. HGF was predicted to elicit resistance in one lung cancer cell line NCI?H596. This analysis suggests that unlike in the EGFR and BRAF inhibitor models, HGF is not a major contributing cytokine to eliciting resistance to a cytotoxic drug: docetaxel. (c) We created a heatmap of pathway-stromal mapping scores for the docetaxel model, computed one-sided p-values according to a null model, and we highlighted scores associated with either drug resistance or sensitivity that passed an FDR threshold of 5\% and 10\%. (c) We created a heatmap of interaction scores for the model of docetaxel treated co-cultures.}
\label{fig:cytokinemappings:Docetaxel:a}
\end{figure}
\end{document}