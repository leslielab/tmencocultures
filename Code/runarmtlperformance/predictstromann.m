
function [prediction, predictionid] = predictstromann(S_test, S_train, cell_line, cancer_testid, Y);
% find the nearest neighbor in input space S
num_train=size(S_train,1);

if num_train > 1
    idx = knnsearch(S_train, S_test);
    idxx= cell_line(idx); 
    predict = Y(cancer_testid, idxx);
    
    
    if ~isnan(predict)
    
    prediction = predict;
    
    predictionid = idxx;      
    
    else
        
        
        S_train(idx,:)=[];
        cell_line(idx,:)=[];
        
        [prediction, predictionid] = predictNNstroma(S_test, S_train, cell_line, cancer_testid, Y);
        
        
    end
else
    
    idxx = cell_line;
    predictionid = idxx;
    prediction = Y(cancer_testid, idxx);
   
    

end
