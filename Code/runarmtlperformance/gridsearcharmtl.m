
function [lambda_bestperformance rho2_bestperformance] = gridsearcharmtl(drug_group, lambdas, rsL2, rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway, nfolds, path, gridsearchid)

%Train the least squares regression

[L2 L1] = ndgrid(rsL2, lambdas);
params = [L1(:) L2(:)];
num_param=size(params,1);
num_lambdas=size(lambdas,2);
num_rsL2=size(rsL2,2);

% performance vector
performance = zeros(num_rsL2, num_lambdas);
standarddev = zeros(num_rsL2, num_lambdas);
mseboxplot = zeros(nfolds, num_param);
mseboxplotnames = cell(num_param, 1);
for param_ix =  1:num_param
    lambda = params(param_ix, 1);
    rho2 = params(param_ix, 2);
    rho1 = 0;
    savelogical = 0;


datasource = 'TMEN';
fprintf('running %s ARMTL %d out of %d total\n', datasource, param_ix, num_param);

   models_eval = armtlgridsearchtenfoldcv(drug_group,  rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway, lambda, rho1, rho2, nfolds, path, savelogical);

   foldmse = models_eval.foldmse;
   meanmse = mean(foldmse);
   sd = std(foldmse)/sqrt(nfolds);
   performance(param_ix)=meanmse;
   standarddev(param_ix)=sd;
   mseboxplot(:,param_ix)=foldmse;
   mseboxplotname = sprintf('\\rho_{1} = %g, \\rho_{2} = %g', params(param_ix,1), params(param_ix,2));
   mseboxplotnames{param_ix} = mseboxplotname;
 
end

bestperformance=min(min(performance));
bestperformanceids=find(performance==bestperformance);
bestperformanceparamid=bestperformanceids(1);
lambda_bestperformance = params(bestperformanceparamid, 1);
rho2_bestperformance = params(bestperformanceparamid, 2);

mkdir(sprintf('%s/Results/plots/GridSearch/rescue%dcyto%dnorm%dpathway%d/', path, rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway));

save(sprintf('%s/Results/plots/GridSearch/rescue%dcyto%dnorm%dpathway%d/%s_performance_%g.mat', path, rescuenormalization, cytokinearraynumber,cytokinenormalization, pathway, drug_group,  gridsearchid), 'performance', 'standarddev', 'mseboxplot', 'mseboxplotnames', 'lambdas', 'rsL2');

h1=figure;
surf(performance');

title(sprintf('%s drug group model performance', drug_group));


xtick=1:num_rsL2;
xticklabel= rsL2;
set(gca,'xtick',xtick);
set(gca,'xticklabel',xticklabel);


ytick=1:num_lambdas;
yticklabel= lambdas;
set(gca,'ytick',ytick);
set(gca,'yticklabel',yticklabel);


xlabel('\rho_2');
ylabel('\rho_1');
zlabel('mean squared error');



print(h1, '-dpdf', sprintf('%s/Results/plots/GridSearch/rescue%dcyto%dnorm%dpathway%d/%s_gridsearch_%g.pdf', path, rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway, drug_group,  gridsearchid));

close(h1)
num_rsL2=size(rsL2,2);
rho1 = repmat(lambdas, num_rsL2,1);
rho1mse = performance;
rho1sd = standarddev;



h2 = figure; 
errorbar(rho1, rho1mse, rho1sd, 'mo', 'LineWidth',1.3,'MarkerEdgeColor','k','MarkerFaceColor','cyan','MarkerSize',7);
xlabel('\rho_1')
ylabel('mean squared error');
title(sprintf('Mean Squared Error ordered by Rho 1 for %s', drug_group));
print(h2, '-dpdf', sprintf('%s/Results/plots/GridSearch/rescue%dcyto%dnorm%dpathway%d/%s_mseplotrho1_%g.pdf', path, rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway, drug_group,  gridsearchid));

close(h2);


num_lambdas=size(lambdas,2);
rho2 = repmat(rsL2, num_lambdas,1);
rho2mse = performance';
rho2sd = standarddev';


h3 = figure; 
errorbar(rho2, rho2mse, rho2sd, 'mo', 'LineWidth',1.3,'MarkerEdgeColor','k','MarkerFaceColor','cyan','MarkerSize',7);
xlabel('\rho_2')
ylabel('mean squared error');
title(sprintf('Mean Squared Error ordered by Rho 2 for %s', drug_group));
print(h3, '-dpdf', sprintf('%s/Results/plots/GridSearch/rescue%dcyto%dnorm%dpathway%d/%s_mseplotrho2_%g.pdf', path, rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway, drug_group,  gridsearchid));

close(h3)




% get position of current xtick labels

h4 = figure;

boxplot(mseboxplot,mseboxplotnames, 'labelorientation', 'inline');
hL=findobj(gca,'Type','text');
set(hL, 'Interpreter', 'tex');
set(hL, 'FontSize', 10);
hold on;
xindex= 1:num_param;
meanmse = performance(:);
meansd = standarddev(:);
errorbar(xindex, meanmse, meansd, 'mo', 'LineWidth',1.3,'MarkerEdgeColor','k','MarkerFaceColor','cyan','MarkerSize',7);
title(sprintf('%s Parameter Mean Squared Errors', drug_group));
print(h4, '-dpdf', sprintf('%s/Results/plots/GridSearch/rescue%dcyto%dnorm%dpathway%d/%s_boxplot_%g.pdf', path, rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway, drug_group,  gridsearchid));
close(h4)




end