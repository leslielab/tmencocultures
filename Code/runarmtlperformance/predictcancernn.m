
function [prediction, predictionid] = predictcancernn(C_test, C_train, cell_line, stroma_testid, Y);
% find the nearest neighbor in input space C

num_train=size(C_train,1);
if num_train > 1
    idx = knnsearch(C_train, C_test);
    idxx= cell_line(idx); 
    predict = Y(idxx, stroma_testid);
    
    
    if ~isnan(predict)
    
    prediction = predict;
    
    predictionid = idxx;      
    
    else
        
        
        C_train(idx,:)=[];
        cell_line(idx,:)=[];
        
        [prediction, predictionid] = predictNNcancer(C_test, C_train, cell_line, stroma_testid, Y);
        
        
    end
else
    
    idxx = cell_line;
    predictionid = idxx;
    prediction = Y(idxx, stroma_testid);
   
    

end

