function [mse msevec]= eval_mse(Y, X, W)


y_pred = X * W;
msevec = ((y_pred - Y).^2);
mse=sum(msevec)/length(msevec);
end