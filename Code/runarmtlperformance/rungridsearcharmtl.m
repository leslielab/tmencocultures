path = '.';
datasource = 'TMEN';

drugs = {'Erlotinib', 'BIBW2992', 'Gefitinib', 'CL-387785', 'Lapatinib', 'Canertinib', 'PLX4720', 'PD184352', 'SB590885', 'AZD6244', 'VEGFR', 'BCRABL', 'Docetaxel', 'EGFR', 'BRAF'};

rescuenormalization=2;
cytokinearraynumber=1;
cytokinenormalization=88;
pathway=20;
number_folds=10;


Lambdas={};
Rhos={};

lambda=[0 0.00001 0.0001 0.001 0.01 0.1 1 10 100 1000 10000];
rho=[0 0.00001 0.0001 0.001 0.01 0.1 1 10 100 1000 10000];


for drugid = 1:size(drugs,2)
drug=drugs{drugid};
[lambda_bestperformance1 rho_bestperformance1] = gridsearcharmtl(drug, lambda, rho, rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway, number_folds, path, 1);
if(lambda_bestperformance1==0)
fprintf(sprintf('%s lambda = 0\n', drug))
lambda_bestperformance1=lambda(2);
end
if(rho_bestperformance1==0)
fprintf(sprintf('%s rho = 0\n', drug))
rho_bestperformance1=rho(2);
end


step=lambda_bestperformance1;
nstart=0;
nend=step*10;
lambda2=nstart:step:nend;


step=rho_bestperformance1;
nstart=0;
nend=step*10;
rho2=nstart:step:nend;

[lambda_bestperformance2 rho_bestperformance2] = gridsearcharmtl(drug, lambda2, rho2, rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway, number_folds, path, 2);
if(lambda_bestperformance2==0)
lambda_bestperformance2=lambda2(2);
end
if(rho_bestperformance2==0)
rho_bestperformance2=rho2(2);
end


if (lambda_bestperformance2 >= 1 &&  lambda_bestperformance2 < 10)
step = 0.1; 
elseif (lambda_bestperformance2 >= 10 &&  lambda_bestperformance2 < 100)    
step = 1;
elseif (lambda_bestperformance2 >= 100 &&  lambda_bestperformance2 < 1000)    
step = 10;
elseif (lambda_bestperformance2 >= 1000 &&  lambda_bestperformance2 < 10000)    
step = 100;
elseif (lambda_bestperformance2 >= 10000 &&  lambda_bestperformance2 < 100000)    
step = 1000;
elseif (lambda_bestperformance2 >= 100000 &&  lambda_bestperformance2 < 1000000)    
step = 10000;
else
lambdadecimalplace=decimalpoint(lambda_bestperformance2);
step=10^-(lambdadecimalplace+1);
end

nstart=lambda_bestperformance2-(step*4);
nend=lambda_bestperformance2+(step*4);
lambda3=nstart:step:nend;


rhodecimalplace=decimalpoint(rho_bestperformance2);
step=10^-(rhodecimalplace+1);

nstart=rho_bestperformance2-(step*4);
nend=rho_bestperformance2+(step*4);
rho3=nstart:step:nend;

Lambdas=[Lambdas; lambda3];
Rhos=[Rhos; rho3];

end


save('./Processed/Parameters.mat', 'Lambdas', 'Rhos')
