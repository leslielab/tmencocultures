

function models_eval = armtltenfold(drug_group,  rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway, lambda, rho1, rho2, nfolds, path, savelogical)


input = sprintf('%s/Processed/coculturegroupsrescue%dcyto%dnorm%dpathway%d/%s_drug_group.mat',  path, rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway, drug_group);

load(input);

number_tasks=length(data.X);
t=number_tasks;
R = eye(t) - ones(t)/t;

%rng('default');     % reset random generator.Available from Matlab 2011.
opts.init = 0;      % guess start point from data. 
opts.tFlag = 1;     % terminate after relative objective value does not changes much.
opts.tol = 10^-5;   % tolerance. 
opts.maxIter = 500; % maximum iteration number of optimization.



opts.rho_L2=rho2;
foldMSE=[];
MSE=[];
foldSpearman=[];
Spearman=[];

SquaredError=[];
%%% There can be other choices of the R (for different structures).
%R = eye(task_num); % ridge penalty.
%R = zeros(t,t-1);H(1:(t+1):end)=1;H(2:(t+1):end)=-1; % order structure
%R = eye (task_num) - ones (task_num) / task_num;  %regularized MTL penalty
models= cell(nfolds,1);

for foldid = 1:nfolds
numbertasks = size(data.Y,2);

Xtest = cell(numbertasks, 1);
Ytest = cell(numbertasks, 1);
Xtrain = cell(numbertasks, 1);
Ytrain = cell(numbertasks, 1);
testids = cell(numbertasks, 1);
trainids = cell(numbertasks, 1);

for t = 1:numbertasks;
n=size(data.Y{t},1);
step=floor(n/nfolds);
nstart=1;
nend=n+1;
ids=nstart:step:nend;
ids(end)=nend;
allids = 1:n;

    testids{t}=data.holdout{t}(ids(foldid):(ids(foldid+1)-1));
    trainids{t}=setdiff(allids, testids{t})';
    %Get the new set of Ys
    Ytest{t} = data.Y{t}(testids{t});
    Ytrain{t} = data.Y{t}(trainids{t});
    %Get the new set of Xs features of the examples
    Xtest{t}=data.X{t}(testids{t}, :);
    Xtrain{t}=data.X{t}(trainids{t}, :);
    
    
    
end    
    

[W_est funcVal] = Least_SRMTL(Xtrain, Ytrain, R, lambda, rho1, opts);

[mse_mean mse_vec]= eval_MTL_mse(Ytest, Xtest, W_est);

spearmans = eval_MTL_spearman(Ytest, Xtest, W_est);

%plot(funcVal)

W_avg=(lambda/(rho2+lambda))*(1/t)*mean(W_est,2);

%W_t=W_est(:,4)-W;
    model.Ytrain = Ytrain;
    model.Ytest = Ytest;
    model.Xtrain= Xtrain;
    model.Xtest = Xtest;
    model.trainids = trainids;
    model.testids = testids;
    model.lambda = lambda;
    model.opts = opts;
    model.fit.beta = W_est;
    model.fit.funVal = funcVal;
    model.Waverage = W_avg;

    
    Ypredict = cell(numbertasks, 1);
    for t = 1:numbertasks
    Ypredict{t} = Xtest{t} * W_est(:, t);
    end
    
    model.Ypredict = Ypredict;      
    models{foldid} = model;
    
    
    MSE = [MSE mse_mean];
    
    Spearman=[Spearman spearmans];

    
    SquaredError=[SquaredError; mse_vec];
    
    foldmeanmse = mean(mse_vec);
    
    foldMSE=[foldMSE; foldmeanmse];
    
    
    foldmeanspearmans = mean(spearmans);
    
    foldSpearman = [foldSpearman; foldmeanspearmans];
end


    taskMSE = mean(MSE');
    Spearman=Spearman';
    taskSpearman=mean(Spearman);

    models_eval.mse = MSE(:);
    models_eval.spearman = Spearman(:);
    models_eval.taskmse = taskMSE';
    models_eval.taskspearman = taskSpearman';
    models_eval.foldmse = foldMSE;
    models_eval.foldspearman = foldSpearman;
    models_eval.se = SquaredError;
    
models_data.processdata=data;
models_data.Y=Y;
models_data.C=cancerpathways;
models_data.S=cytokinearray;



   
   if(savelogical)
   
   mkdir(sprintf('%s/models/Pathway_Models_JointGroup/%s/', path, drug_group));

   filename = sprintf('%s/models/Pathway_Models_JointGroup/%s/models_rescue%d_cyto%d_norm%d_pathway%d.mat', path, drug_group,  rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway);
   parsave(filename, models, models_eval, models_data, lambda, rho1, rho2);
   fprintf('saved %s \n', filename);
   end
    
    
    
end

