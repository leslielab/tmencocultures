function [taskspearman OptimalParameter]= arleastrnestedtenfoldcv(drug_group, rsL2, rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway, nfolds, path)

input = sprintf('%s/Processed/coculturegroupsrescue%dcyto%dnorm%dpathway%d/%s_drug_group.mat',  path, rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway, drug_group);


load(input);
data.Y=data.Y';
data.X=data.X';
data.holdout=data.holdout';
data.info=data.info';

datasource = 'TMEN';

%rng('default');     % reset random generator.Available from Matlab 2011.
opts.init = 0;      % guess start point from data. 
opts.tFlag = 1;     % terminate after relative objective value does not changes much.
opts.tol = 10^-5;   % tolerance. 
opts.maxIter = 500; % maximum iteration number of optimization.


MedianParameters=nan(nfolds,1);
%%% There can be other choices of the R (for different structures).
%R = eye(task_num); % ridge penalty.
%R = zeros(t,t-1);H(1:(t+1):end)=1;H(2:(t+1):end)=-1; % order structure
%R = eye (task_num) - ones (task_num) / task_num;  %regularized MTL penalty

for foldid = 1:nfolds
fprintf('running %s LeastR : Outer Fold %d\n', datasource, foldid);


numbertasks = size(data.Y,1);

testids = cell(numbertasks, 1);
trainids = cell(numbertasks, 1);

for t = 1:numbertasks;
n=size(data.holdout{t},1);
step=floor(n/nfolds);
nstart=1;
nend=n+1;
ids=nstart:step:nend;
ids(end)=nend;


    testids{t}=data.holdout{t}(ids(foldid):(ids(foldid+1)-1));
    trainids{t}=setdiff(data.holdout{t}, testids{t});

    
end    
    
    
%[W_est funcVal] = Least_SRMTL(Xtrain, Ytrain, R, lambda, rho1, opts);

traindata.Y = data.Y;
traindata.X = data.X;
traindata.holdout = trainids;
traindata.info = data.info;


%Parameter Search
params = [rsL2(:)];
num_param=size(params,1);
number_tasks=length(data.X);
t=number_tasks;

% performance vector
performance = zeros(num_param, nfolds);
for param_ix =  1:num_param
    lambda = 0;
    rho2 = params(param_ix);
    fprintf('running %s LeastR : Inner Fold %d\n', datasource, param_ix);

   models_eval = arleastrtenfoldcv(drug_group,  traindata, rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway, lambda, rho2, nfolds, path);


   foldmse = models_eval.foldmse;
   performance(param_ix, :)= foldmse;
      
end



[M, I] = min(performance);

MedianParams=median(params(I));
MedianParameters(foldid) = MedianParams;

end

OptimalParameter= median(MedianParameters);
lambda = 0;
rho2 = OptimalParameter;

models_eval=arleastrtenfoldcv(drug_group, data, rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway, lambda, rho2, nfolds, path);



taskmse = models_eval.mse;
taskspearman= models_eval.spearman;



   
end

