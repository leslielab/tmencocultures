function [stromalSpearmans cancerSpearmans taskstromalSpearmans taskcancerSpearmans  stromalMSE cancerMSE]=getNNPerformanceNested(drug_group,  rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway, nfolds, path)

input = sprintf('%s/models/Pathway_Models_JointGroup/%s/models_rescue%d_cyto%d_norm%d_pathway%d.mat', path, drug_group, rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway);
% clearvars -except input

load(input)

%For PLX4720
Y=models_data.Y.rescue;
C=models_data.C.data;
S=models_data.S.data';
%Find the rows and cols of Y that are not nan

datasource = 'TMEN';
fprintf('running %s NN\n', datasource);

numbertasks = size(models_data.Y.rescue,1);
% build model using the optimal parameter 

stromalMSE=[];
stromalSpearmans=[];
cancerMSE=[];
cancerSpearmans=[];
%perform cross validation

for fold = 1:nfolds

taskstromalMSE=[];
taskstromalSpearmans=[];
taskcancerMSE=[];
taskcancerSpearmans=[];

    for t = 1:numbertasks
    
    testids=models_data.Y.holdout{t}(models{fold}.testids{t});
    trainids=models_data.Y.holdout{t}(models{fold}.trainids{t});
    
    
    %Get the new set of Ys
    [trainrow traincol] = ind2sub(size(models_data.Y.rescue{t}), trainids);
    [testrow testcol] = ind2sub(size(models_data.Y.rescue{t}), testids);
    
    %Get the stroma training
    stroma = unique(traincol);
    S_train = S(stroma,:);
    
    %Get the cancer training
    cancer = unique(trainrow);
    C_train = C(cancer,:);
    

    %Get the cancer and stroma test
    S_test = S(testcol, :);
    C_test = C(testrow,:);
    
    %Get ytest and ytrain
    Ytest = models{fold}.Ytest{t};
    Ytrain = models{fold}.Ytrain{t};

    
    %Convert ids into the training and test matrices
        trainrowids = arrayfun(@(x) find(cancer == x), trainrow);
        traincolids = arrayfun(@(x) find(stroma == x), traincol);
        testrowids = arrayfun(@(x) find(cancer == x), testrow);
        testcolids = arrayfun(@(x) find(stroma == x), testcol);

    %Get the predictions from stroma
    
    Number_test = size(S_test, 1);
    Y_predict_stroma=zeros(Number_test, 1);
    idxS=zeros(Number_test, 1);

for j = 1:Number_test   
    Strain = S_train;
    Strain(testcolids(j), :) = [];
    stromann=stroma;
    stromann(find(stromann == testcol(j)),:)=[];
    
    
    [prediction predictids]=predictstromann(S_test(j, :), Strain, stromann, testrow(j, :), Y{t});
    Y_predict_stroma(j)=prediction;
    idxS(j)=predictids;
end



    %Get the predictions from cancer

    Number_test = size(C_test, 1);
    Y_predict_cancer=zeros(Number_test, 1);
    idxC=zeros(Number_test, 1);

for k = 1:Number_test
    
    Ctrain = C_train;
    Ctrain(testrowids(k), :) = [];
    cancernn=cancer;
    cancernn(find(cancernn == testcol(k)),:)=[];
   
    [prediction predictids] = predictcancernn(C_test(k, :), Ctrain, cancernn, testcol(k, :), Y{t});
    Y_predict_cancer(k)=prediction;
    idxC(k)=predictids;     
end
    
  size(find(isnan(Y_predict_stroma)));
  size(find(isnan(Y_predict_cancer)));
   %Here remove nans
        
    idnans = find(isnan(Y_predict_cancer));
    Y_predict_cancer(idnans) = [];
    Y_test_cancer = Ytest;
    Y_test_cancer(idnans) = [];

    idnans = find(isnan(Y_predict_stroma));
    Y_predict_stroma(idnans) = [];
    Y_test_stroma = Ytest;
    Y_test_stroma(idnans) = [];
    

    stroma_error=mean((Y_test_stroma - Y_predict_stroma).^2);
    taskstromalMSE = [taskstromalMSE; stroma_error];


    stromalSpearman = corr(Y_test_stroma,Y_predict_stroma,'Type','Spearman');
    taskstromalSpearmans = [taskstromalSpearmans; stromalSpearman];
    

    cancer_error=mean((Y_test_cancer - Y_predict_cancer).^2);
    taskcancerMSE = [taskcancerMSE; cancer_error];


    cancerSpearman = corr(Y_test_cancer,Y_predict_cancer,'Type','Spearman');
    taskcancerSpearmans = [taskcancerSpearmans; cancerSpearman];
    
end

stromalMSE=[stromalMSE taskstromalMSE];
stromalSpearmans=[stromalSpearmans taskstromalSpearmans];
cancerMSE=[cancerMSE taskcancerMSE];
cancerSpearmans=[cancerSpearmans taskcancerSpearmans];
    
    
    
    
end

stromalSpearmans=stromalSpearmans';
cancerSpearmans=cancerSpearmans';

stromalMSE = stromalMSE(:);
taskstromalSpearmans=mean(stromalSpearmans)';
stromalSpearmans=stromalSpearmans(:);
cancerMSE=cancerMSE(:);
taskcancerSpearmans=mean(cancerSpearmans)';
cancerSpearmans=cancerSpearmans(:);



save(sprintf('%s/Results/Performance/NearestNeighbor/%s_nearestneighbor.mat', path, drug_group),  'stromalMSE', 'cancerMSE', 'stromalSpearmans', 'cancerSpearmans', 'taskstromalSpearmans', 'taskcancerSpearmans');  

end
