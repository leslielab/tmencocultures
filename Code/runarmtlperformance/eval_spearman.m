function spearmancorrelation = eval_spearman(Y, X, W)


y_pred = X * W;
spearmancorrelation = corr(y_pred, Y, 'type', 'Spearman');

end