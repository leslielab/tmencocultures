
function [mse spearman taskspearman OptimalParameters]= armtlnestedtenfoldcv(drug_group, lambdas, rsL2, rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway, nfolds, path)



input = sprintf('%s/Processed/coculturegroupsrescue%dcyto%dnorm%dpathway%d/%s_drug_group.mat',  path, rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway, drug_group);


datasource = 'TMEN';


load(input);

data.Y=data.Y';
data.X=data.X';
data.holdout=data.holdout';
data.info=data.info';
% Parameter search

%lambdas = [0 0.001 0.01 0.1 1 10 50 100];
%rho_L2s = [0 0.001 0.01 0.1 1 10 50 100];


%[L1 L2] = ndgrid(lambdas, rho_L2s);
%params = [L1(:) L2(:)];
number_tasks=length(data.X);
t=number_tasks;
R = eye(t) - ones(t)/t;

%rng('default');     % reset random generator.Available from Matlab 2011.
opts.init = 0;      % guess start point from data. 
opts.tFlag = 1;     % terminate after relative objective value does not changes much.
opts.tol = 10^-5;   % tolerance. 
opts.maxIter = 500; % maximum iteration number of optimization.



%%% There can be other choices of the R (for different structures).
%R = eye(task_num); % ridge penalty.
%R = zeros(t,t-1);H(1:(t+1):end)=1;H(2:(t+1):end)=-1; % order structure
%R = eye (task_num) - ones (task_num) / task_num;  %regularized MTL penalty
MedianParameters=nan(nfolds,2);

for foldid = 1:nfolds
fprintf('running %s ARMTL : Outer Fold %d\n', datasource, foldid);

numbertasks = size(data.Y,1);

testids = cell(numbertasks, 1);
trainids = cell(numbertasks, 1);

for t = 1:numbertasks;
n=size(data.holdout{t},1);
step=floor(n/nfolds);
nstart=1;
nend=n+1;
ids=nstart:step:nend;
ids(end)=nend;


    testids{t}=data.holdout{t}(ids(foldid):(ids(foldid+1)-1));
    trainids{t}=setdiff(data.holdout{t}, testids{t});

    
end    
    
    
%[W_est funcVal] = Least_SRMTL(Xtrain, Ytrain, R, lambda, rho1, opts);

traindata.Y = data.Y;
traindata.X = data.X;
traindata.holdout = trainids;
traindata.info = data.info;

%Parameter Grid Search
[L2 L1] = ndgrid(rsL2, lambdas);
params = [L1(:) L2(:)];
num_param=size(params,1);
num_lambdas=size(lambdas,2);
num_rsL2=size(rsL2,2);

% performance vector
msefoldperformance = zeros(nfolds, num_param);

performance = zeros(num_rsL2, num_lambdas);
standarddev = zeros(num_rsL2, num_lambdas);

mseboxplotnames = cell(num_param, 1);
for param_ix =  1:num_param
    lambda = params(param_ix, 1);
    rho2 = params(param_ix, 2);
    rho1 = 0;
    savelogical = 0;

    fprintf('running %s ARMTL : Inner Fold %d\n', datasource, param_ix);

    
   models_eval = armtltenfoldcv(drug_group,  traindata, rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway, lambda, rho1, rho2, nfolds, path, savelogical);

   foldmse = models_eval.foldmse;
   meanmse = mean(foldmse);
   sd = std(foldmse)/sqrt(nfolds);
   performance(param_ix)=meanmse;
   standarddev(param_ix)=sd;
   msefoldperformance(:,param_ix)=foldmse;
   mseboxplotname = sprintf('\\rho_{1} = %g, \\rho_{2} = %g', params(param_ix,1), params(param_ix,2));
   mseboxplotnames{param_ix} = mseboxplotname;

end



[M, I] = min(msefoldperformance');
MedianParams=median(params(I, :));
MedianParameters(foldid,:)= MedianParams;

    
mkdir(sprintf('%s/Results/plots/GridSearch/%s/', path, drug_group));

save(sprintf('%s/Results/plots/GridSearch/%s/%s_performance_%d.mat', path, drug_group, drug_group, foldid), 'performance', 'standarddev', 'msefoldperformance', 'mseboxplotnames', 'lambdas', 'rsL2');

h1=figure;
surf(performance');
x=1;
y=1;
z=1;
view([x,y,z]) 
title(sprintf('%s drug group model performance fold %g', drug_group, foldid));


xtick=1:num_rsL2;
xticklabel= rsL2;
set(gca,'xtick',xtick);
set(gca,'xticklabel',xticklabel);


ytick=1:num_lambdas;
yticklabel= lambdas;
set(gca,'ytick',ytick);
set(gca,'yticklabel',yticklabel);


xlabel('\rho_2');
ylabel('\rho_1');
zlabel('mean squared error');



print(h1, '-dpdf', sprintf('%s/Results/plots/GridSearch/%s/%s_gridsearch_%d.pdf', path, drug_group, drug_group, foldid));

close(h1)
num_rsL2=size(rsL2,2);
rho1 = repmat(lambdas, num_rsL2,1);
rho1mse = performance;
rho1sd = standarddev;



h2 = figure; 
errorbar(rho1, rho1mse, rho1sd, 'mo', 'LineWidth',1.3,'MarkerEdgeColor','k','MarkerFaceColor','cyan','MarkerSize',7);
xlabel('\rho_1')
ylabel('mean squared error');
title(sprintf('Mean Squared Error ordered by Rho 1 for %s fold %g', drug_group, foldid));
print(h2, '-dpdf', sprintf('%s/Results/plots/GridSearch/%s/%s_mseplotrho1_%d.pdf', path, drug_group, drug_group, foldid));

close(h2);


num_lambdas=size(lambdas,2);
rho2 = repmat(rsL2, num_lambdas,1);
rho2mse = performance';
rho2sd = standarddev';


h3 = figure; 
errorbar(rho2, rho2mse, rho2sd, 'mo', 'LineWidth',1.3,'MarkerEdgeColor','k','MarkerFaceColor','cyan','MarkerSize',7);
xlabel('\rho_2')
ylabel('mean squared error');
title(sprintf('Mean Squared Error ordered by Rho 2 for %s fold %g', drug_group, foldid));
print(h3, '-dpdf', sprintf('%s/Results/plots/GridSearch/%s/%s_mseplotrho2_%d.pdf', path, drug_group, drug_group, foldid));

close(h3)




% get position of current xtick labels

h4 = figure;

boxplot(msefoldperformance,mseboxplotnames, 'labelorientation', 'inline');
hL=findobj(gca,'Type','text');
set(hL, 'Interpreter', 'tex');
set(hL, 'FontSize', 7);
hold on;
xindex= 1:num_param;
meanmse = performance(:);
meansd = standarddev(:);
errorbar(xindex, meanmse, meansd, 'mo', 'LineWidth',1.3,'MarkerEdgeColor','k','MarkerFaceColor','cyan','MarkerSize',7);
title(sprintf('%s Parameter Mean Squared Errors fold %g', drug_group, foldid));
print(h4, '-dpdf', sprintf('%s/Results/plots/GridSearch/%s/%s_boxplot_%d.pdf', path, drug_group, drug_group, foldid));
close(h4)



end



OptimalParameters=median(MedianParameters);
lambda = OptimalParameters(1);
rho2 = OptimalParameters(2);
rho1 = 0;
savelogical = 1;
    
models_eval=armtltenfoldcv(drug_group, data, rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway, lambda, rho1, rho2, nfolds, path, savelogical);

mse = models_eval.mse;
spearman= models_eval.spearman;
taskspearman= models_eval.taskspearman;

end






