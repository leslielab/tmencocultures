

function models_eval = arleastrtenfoldcv(drug_group, data, rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway, lambda, rho2, nfolds, path)
% Parameter search

%lambdas = [0 0.001 0.01 0.1 1 10 50 100];
%rho_L2s = [0 0.001 0.01 0.1 1 10 50 100];


%[L1 L2] = ndgrid(lambdas, rho_L2s);
%params = [L1(:) L2(:)];
number_tasks=length(data.X);
t=number_tasks;


%rng('default');     % reset random generator.Available from Matlab 2011.
opts.init = 0;      % guess start point from data. 
opts.tFlag = 1;     % terminate after relative objective value does not changes much.
opts.tol = 10^-5;   % tolerance. 
opts.maxIter = 500; % maximum iteration number of optimization.
opts.rsL2=rho2;



foldMSE=[];
MSE=[];
Spearman=[];
meansquarederror=[];
%%% There can be other choices of the R (for different structures).
%R = eye(task_num); % ridge penalty.
%R = zeros(t,t-1);H(1:(t+1):end)=1;H(2:(t+1):end)=-1; % order structure
%R = eye (task_num) - ones (task_num) / task_num;  %regularized MTL penalty

for foldid = 1:nfolds
numbertasks = number_tasks;



taskMSE=[];
taskSpearman=[];
taskmeansquarederror=[];


for t = 1:numbertasks;
n=size(data.holdout{t},1);
step=floor(n/nfolds);
nstart=1;
nend=n+1;
ids=nstart:step:nend;
ids(end)=nend;

    testids=data.holdout{t}(ids(foldid):(ids(foldid+1)-1));
    trainids=setdiff(data.holdout{t}, testids);
    %Get the new set of Ys
    Ytest = data.Y{t}(testids);
    Ytrain = data.Y{t}(trainids);
    %Get the new set of Xs features of the examples
    Xtest=data.X{t}(testids, :);
    Xtrain=data.X{t}(trainids, :);
    
    
    start = tic();
    [W_individual b] = LeastR(Xtrain, Ytrain, lambda, opts); 
    end_time = toc(start);
    
    
    [mse se]=eval_mse(Ytest, Xtest, W_individual);

    spearman = eval_spearman(Ytest, Xtest, W_individual);
    
    
taskMSE=[taskMSE; mse];
taskSpearman=[taskSpearman; spearman];
taskmeansquarederror=[taskmeansquarederror; se];


end    
    
MSE=[MSE taskMSE];
Spearman=[Spearman taskSpearman];
meansquarederror=[meansquarederror; taskmeansquarederror];
foldmeanse=mean(taskmeansquarederror);  
foldMSE=[foldMSE; foldmeanse];



end

    taskMSE = mean(MSE');    
    taskSpearman=mean(Spearman');
    MeanSquaredError = meansquarederror; 
    
    
    models_eval.mse = taskMSE';
    models_eval.foldmse = foldMSE;
    models_eval.spearman = taskSpearman';
    models_eval.meansquarederror = MeanSquaredError;
    
    
end

