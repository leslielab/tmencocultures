
path='.';
datasource = 'TMEN';

rescuenormalization=2;
cytokinearraynumber=1;
cytokinenormalization=88;
pathway=20;
number_folds=10;

drugs = {'Erlotinib', 'BIBW2992', 'Gefitinib', 'CL-387785', 'Lapatinib', 'Canertinib', 'PLX4720', 'PD184352', 'SB590885', 'AZD6244', 'VEGFR', 'BCRABL', 'Docetaxel', 'EGFR', 'BRAF'};

load('Parameters.mat')


SpearmanLeastR=[];
SpearmanARMTL=[];
ParametersLeastR=[];
ParametersARMTL=[];
SpearmanStromal=[];
SpearmanCancer=[];


RhosLeastR = [0.0001:0.0001:0.0009 0.001:0.001:0.009 0.01:0.01:0.09 0.1:0.1:0.9 1:1:9 10:10:100];

mkdir(sprintf('%s/Results/plots/ARMTLvsLeastRPerformance/', path));
mkdir(sprintf('%s/Results/plots/ARMTLvsNNPerformance/', path));


for i = 1:size(drugs,2)

fprintf('running %s : %s\n', datasource, drugs{i});

[arMSE arSpearmans taskspearmanARMTL paramsMTL] = getMTLPerformanceNested(drugs{i}, Lambdas{i}, Rhos{i}, rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway, number_folds, path);
[taskspearmanLeastR paramsLeastR]= getLeastRPerformanceNested(drugs{i}, RhosLeastR, rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway, number_folds, path);
[stromalSpearmans cancerSpearmans taskstromalSpearmans taskcancerSpearmans  stromalMSE cancerMSE]  = getNNPerformanceNested(drugs{i},  rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway, number_folds, path);


SpearmanLeastR=[SpearmanLeastR; taskspearmanLeastR];
SpearmanARMTL=[SpearmanARMTL; taskspearmanARMTL];
ParametersLeastR=[ParametersLeastR; paramsLeastR];
ParametersARMTL=[ParametersARMTL; paramsMTL];
SpearmanStromal=[SpearmanStromal; taskstromalSpearmans];
SpearmanCancer=[SpearmanCancer; taskcancerSpearmans];



%Do the Spearman box plot and scatter plot and the SE and MSE boxplots
SpearmanCorrelation=[cancerSpearmans stromalSpearmans arSpearmans];

modelnames={'Cancer NN', 'Stroma NN', 'Affinity Regression'};

h1=figure;
boxplot(SpearmanCorrelation,'labels', modelnames);


title(sprintf('%s : Affinity Regression Performance', drugs{i}));
xlabel('Models');
ylabel('Spearman Correlation');


print(h1, '-dpdf', sprintf('%s/Results/plots/ARMTLvsNNPerformance/%s_ARvsNN_Performance_Spearmans_Boxplot.pdf',path, drugs{i}));

close(h1);



[ps hs]= signrank(arSpearmans, stromalSpearmans,  'tail', 'right');

h2 = figure;

plot(arSpearmans, stromalSpearmans,  '.', 'MarkerSize', 20, 'Color', 'blue');
hold on;
x = [-1 1];
y = [-1 1];
plot(x, y, 'Color', 'black')

legend(sprintf('P-value = %2.2e', ps), 'location', 'southeast');

title(sprintf('%s : Affinity Regression Performance', drugs{i}));
xlabel('Affinity Regression');
ylabel('Stromal Nearest Neighbor');


print(h2, '-dpdf', sprintf('%s/Results/plots/ARMTLvsNNPerformance/%s_ARvsStromalNN_Performance_Stromal_Spearmans_Scatterplot.pdf', path, drugs{i}));

close(h2);


[pc hc]= signrank(arSpearmans, cancerSpearmans,  'tail', 'right');

h3 = figure;

plot(arSpearmans, cancerSpearmans,  '.', 'MarkerSize', 20, 'Color', 'blue');
hold on;
x = [-1 1];
y = [-1 1];
plot(x, y, 'Color', 'black')

legend(sprintf('P-value = %2.2e', pc), 'location', 'southeast');

title(sprintf('%s : Affinity Regression Performance', drugs{i}));
xlabel('Affinity Regression');
ylabel('Cancer Nearest Neighbor');


print(h3, '-dpdf', sprintf('%s/Results/plots/ARMTLvsNNPerformance/%s_ARvsStromalNN_Performance_Cancer_Spearmans_Scatterplot.pdf', path, drugs{i}));

close(h3);



MeanSquaredError=[cancerMSE stromalMSE arMSE];

modelnames={'Cancer NN', 'Stroma NN', 'Affinity Regression'};

h4=figure;
boxplot(MeanSquaredError,'labels', modelnames);


title(sprintf('%s : Affinity Regression Performance', drugs{i}));
xlabel('Models');
ylabel('Mean Squared Error');


print(h4, '-dpdf', sprintf('%s/Results/plots/ARMTLvsNNPerformance/%s_ARvsNN_Performance_MSE.pdf',path, drugs{i}));

close(h4);


save(sprintf('%s/Results/plots/ARMTLvsLeastRPerformance/ARMTLvsLeastRPerformance.mat', path), 'ParametersARMTL', 'ParametersLeastR', 'SpearmanARMTL', 'SpearmanLeastR')
save(sprintf('%s/Results/plots/ARMTLvsNNPerformance/ARMTLvsNNPerformance.mat', path),  'SpearmanARMTL','SpearmanStromal', 'SpearmanCancer')


end


save(sprintf('%s/Results/plots/ARMTLvsLeastRPerformance/ARMTLvsLeastRPerformance.mat', path), 'ParametersARMTL', 'ParametersLeastR', 'SpearmanARMTL', 'SpearmanLeastR')
save(sprintf('%s/Results/plots/ARMTLvsNNPerformance/ARMTLvsNNPerformance.mat', path),  'SpearmanARMTL','SpearmanStromal', 'SpearmanCancer')

load(sprintf('%s/Results/plots/ARMTLvsLeastRPerformance/ARMTLvsLeastRPerformance.mat', path))
load(sprintf('%s/Results/plots/ARMTLvsNNPerformance/ARMTLvsNNPerformance.mat', path))


h1=figure;

[pARMTL hARMTL]= signrank(SpearmanARMTL, SpearmanLeastR,  'tail', 'right');
plot(SpearmanARMTL, SpearmanLeastR, '.','MarkerSize', 20, 'Color', 'Blue')
x = [-1 1];
y = [-1 1];
hold on;
plot(x, y, 'Color', 'black');
legend(sprintf('P-value = %2.2e', pARMTL), 'location', 'southeast');

title('Multi-task vs Linear Regression Performance on Co-cultures')
xlabel('Multi-task Learning');
ylabel('Independent Task Learning');

print(h1, '-dpdf', sprintf('%s/Results/plots/ARMTLvsLeastRPerformance/ARMTLvsLeastR_cocultures_performance.pdf', path));

close(h1);



h2=figure;
[pCancer hCancer]= signrank(SpearmanARMTL, SpearmanCancer,  'tail', 'right');

plot(SpearmanARMTL, SpearmanCancer, '.','MarkerSize', 20, 'Color', 'Blue')
x = [-1 1];
y = [-1 1];
hold on;
plot(x, y, 'Color', 'black');
legend(sprintf('P-value = %2.2e', pCancer), 'location', 'southeast');

title('Affinity Regression Multi-task vs Cancer Nearest Neighbor Performance on Co-cultures')
xlabel('Affinity Regression Multi-task');
ylabel('Cancer Nearest Neighbor');

print(h2, '-dpdf', sprintf('%s/Results/plots/ARMTLvsNNPerformance/ARMTLvsCancerNN_cocultures_performance.pdf', path));

close(h2);


h3=figure;
[pStromal hStromal]= signrank(SpearmanARMTL, SpearmanStromal,  'tail', 'right');

plot(SpearmanARMTL, SpearmanStromal, '.','MarkerSize', 20, 'Color', 'Blue')
x = [-1 1];
y = [-1 1];
hold on;
plot(x, y, 'Color', 'black');
legend(sprintf('P-value = %2.2e', pStromal), 'location', 'southeast');

title('Affinity Regression Multi-task vs Stromal Nearest Neighbor Performance on Co-cultures')
xlabel('Affinity Regression Multi-task');
ylabel('Stromal Nearest Neighbor');

print(h3, '-dpdf', sprintf('%s/Results/plots/ARMTLvsNNPerformance/ARMTLvsStromalNN_cocultures_performance.pdf', path));

close(h3);


