clrred = [228,26,28]/255;
clrblue = [55,126,184]/255;
clrgreen = [77,175,74]/255;
clrpurple = [152,78,163]/255;
clrblack = [0 0 0];


load('./Processed/tissuetype.mat')



subtyperaw=importdata('./Data/ccleannotations/BRAFmutations.txt')
subtypes=[];
for i = 1:length(subtyperaw)
subtype=strsplit(subtyperaw{i}, '_')

subtypes=[subtypes; subtype(1) subtype(2)]

end

skinids = find(strcmp(tissuetype.Type, 'skin'))


skincancers=tissuetype.Cancer(skinids)


setdiff(subtypes(:,1), tissuetype.Cancer)

setdiff(tissuetype.Cancer, subtypes(:,1))

breastcancerset = intersect(skincancers, subtypes(:,1))


skinids = find(strcmp(subtypes(:,2), 'SKIN'));

skinbraf=subtypes(skinids, 1)


braf=subtypes(:,1)

save('./Processed/brafmutations.mat', 'braf', 'skinbraf')


%Boxplot of Melanoma BRAF

load('./Processed/ccle_pathways20.mat')
load('./Processed/brafmutations.mat')
load('./Processed/tissuetype.mat')

pathway = 'PID_RAS_PATHWAY'


skinids = find(strcmp(tissuetype.Type, 'skin'))


skinbrafids = arrayfun(@(x) find(strcmp(filteredpathways.Cancers, x)), skinbraf);


skinbrafids=intersect(skinids, skinbrafids)



sbids = arrayfun(@(x) find(skinids==x), skinbrafids);

wtids=setdiff(1:length(skinids), sbids)


labels = cell(length(skinids), 1)

labels(sbids) = {'BRAF Mutant'}
labels(wtids) = {'Wild Type'}

    
pathwayid = find(strcmp(filteredpathways.Pathways, pathway))

boxplotdata = filteredpathways.data(skinids, pathwayid)


h=figure;
boxplot(boxplotdata, labels, 'colors', clrblack)

hold on

x=zeros(length(boxplotdata),1)

bids=find(strcmp(labels, 'BRAF Mutant'))
wids=find(strcmp(labels, 'Wild Type'))
x(bids) = 2
x(wids) = 1


plot(x, boxplotdata,'.','MarkerSize', 25, 'MarkerEdgeColor', clrblue, 'MarkerFaceColor', clrblue)
 
title(strrep(pathway, '_', '\_'))

print(h, '-dpdf', sprintf('./Data/ccleannotations/%s_brafmelanoma_boxplot.pdf', pathway))
