source("./heatmap.3.R")
library(RColorBrewer)
# define jet colormap
jet.colors <- colorRampPalette(c("#1e1ed7", "blue", "#007FFF", "cyan", "#7FFF7F", "yellow", "#FF7F00", "red", "#d71e1e"))
jetcolors <- jet.colors(1000)[c(seq(1,125, length.out=810),seq(125,250, length.out=63), seq(250,375, length.out=63),seq(375,500, length.out=63), seq(500,625, length.out=63), seq(625,750, length.out=63),seq(750, 875, length.out=63), seq(875,1000,length.out=810))]

#my_palette <- colorRampPalette(c("#06E3E3","#1AF4D0", "#E4F5F2","#EA95ff",  "#E306E3"))(n = 100)

#my_palette <- colorRampPalette(c("#607FB9","#ADC9E0", "#FAFCD2","#EAB57C",  "#BE3A30"))(n = 100)


load("pathwayannotations.rda")

pathway.data <- read.delim("CancerPathwayData.txt", row.names=1, header=T, stringsAsFactors=F, sep='\t', check.names = F)
pathway.cancer.names=rownames(pathway.data)
cancer.names=colnames(pathway.data)


pathwaycolors<-as.matrix(read.table("pathwayannotationcolors.txt", header=F, sep="\t"))


pathwayannotations=as.matrix(pathwayannotations)
pathway.annotations=NULL
for(i in 1:length(pathwayannotations)){
  color=pathwaycolors[which(pathwaycolors[,1]==pathwayannotations[i]),2]
  pathway.annotations=rbind(pathway.annotations, color)
}
rownames(pathway.annotations)=NULL
colnames(pathway.annotations)="Pathway Ontology"


colors<-as.matrix(read.table("colorlabels.txt", header=F, sep="\t"))
annotations<-as.matrix(read.table("primarysite.txt", header=T, sep="\t"))


cancer.cell.lines=as.matrix(colnames(pathway.data))

cancer.cell.lines.annotations=as.matrix(apply(cancer.cell.lines,1, function(x){annotations[which(annotations[,1]==x),2]}))

cancer.annotations=NULL
for(i in 1:length(cancer.cell.lines.annotations)){
  color=colors[which(colors[,1]==cancer.cell.lines.annotations[i]),2]
  cancer.annotations=rbind(cancer.annotations, color)
}
rownames(cancer.annotations)=NULL

cancerlegend=as.matrix(sort(unique(cancer.cell.lines.annotations)))

cancerlegendfill=NULL

for(i in 1:length(cancerlegend)){
  cancerlegendfilling=colors[which(colors[,1] == cancerlegend[i])[1],2]
  cancerlegendfill=rbind(cancerlegendfill, cancerlegendfilling)
}
rownames(cancerlegendfill)=NULL

hRowv <- hclust(dist(pathway.data))
hColv <- hclust(dist(t(pathway.data)))
Rowv <- as.dendrogram(hRowv)
Colv <- as.dendrogram(hColv)

cancer.annotations=as.matrix(cancer.annotations)
colnames(cancer.annotations)<-"Cancer Subtype"
rownames(cancer.annotations)<-NULL


legendtext= rbind(cancerlegend, "", as.matrix(pathwaycolors[,1]))
legendfill= rbind(cancerlegendfill, "white", as.matrix(pathwaycolors[,2]))

cancer.cell.lines.annotation=cancer.cell.lines.annotations


pathwaynames<-rownames(pathway.data)
#pathwaynames<-gsub("PID_", "", pathwaynames)
#pathwaynames<-gsub("_PATHWAY", "", pathwaynames)
#pathwaynames<-gsub("PATHWAY", "", pathwaynames)
#pathwaynames<-gsub("_", " ", pathwaynames)
rownames(pathway.data)<-pathwaynames


pdf("./PathwayHeatmap.pdf")



heatmap.3(pathway.data, Rowv=Rowv, Colv=Colv, main ="Pathway Western Blot", cexCol=0.03,cexRow=0.5,trace="none", scale='row', col=jetcolors, symm=F,symkey=F,symbreaks=T, ColSideColors=cancer.annotations, cexColSideColors=1, ColSideColorsSize=1.5, RowSideColors=t(pathway.annotations), cexRowSideColors=1, RowSideColorsSize=1.5, margins=c(3,15))

legend(.8,.8,legend=legendtext, fill=legendfill, border=FALSE, bty="n", y.intersp = 0.7, cex=0.7)

dev.off()

#write.table(pathwayannotations, file="pathwayannotations.txt", col.names=F, row.names=F, quote=F, sep="\t")
#pathwayannotations<-cbind(as.matrix(rownames(pathway.data)), pathwayannotations)
