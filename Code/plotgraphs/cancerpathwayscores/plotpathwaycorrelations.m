load('./Processed/cancerpathways.mat')
cancers=importdata('Data/usedcancer.xls');

cancerids = arrayfun(@(x) find(strcmp(cancerpathways.Cancers, x)), cancers, 'Uniformoutput', false);
cancerids =  vertcat(cancerids{:});

cancerpathwaysdata=cancerpathways.data(cancerids, :);

pathwaycorrelations=corr(cancerpathwaysdata);

h=figure;
imagesc(pathwaycorrelations)
xlabels=cancerpathways.Pathways;
ylabels=cancerpathways.Pathways;

xdata = 1:length(cancerpathways.Pathways);
ydata = 1:length(cancerpathways.Pathways);
% Set the tick locations and remove the labels 
set(gca,'XTick',xdata,'XTickLabel','') ;
set(gca,'YTick',ydata,'YTickLabel',ylabels) ;
% Define the labels 
% Estimate the location of the labels based on the position 
% of the xlabel 
hx = get(gca,'XLabel');  % Handle to xlabel 
set(hx,'Units','data'); 
pos = get(hx,'Position'); 
y = pos(2); 
% Place the new labels 
for i = 1:size(xlabels,1) 
    t(i) = text(xdata(i),y,xlabels(i,:)); 
end 
set(t,'Rotation',90,'HorizontalAlignment','right')  ;
title('Pathway Correlations')
xlabel('Pathways')
ylabel('Pathways')
print(h,'-dpdf', './Normalization/pathwayscorrelation.pdf')
