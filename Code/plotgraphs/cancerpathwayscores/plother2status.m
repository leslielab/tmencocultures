clrred = [228,26,28]/255;
clrblue = [55,126,184]/255;
clrgreen = [77,175,74]/255;
clrpurple = [152,78,163]/255;
clrblack = [0 0 0];


load('./Processed/tissuetype.mat')



subtyperaw=importdata('./Data/ccleannotations/breastcancersubtypenew.txt')
subtypes1=[];
for i = 2:length(subtyperaw)
subtype=strsplit(subtyperaw{i}, '\t')

subtypes1=[subtypes1; subtype(1) subtype(2)]

end

breastids = find(strcmp(tissuetype.Type, 'breast'))
skinids = find(strcmp(tissuetype.Type, 'skin'))


breastcancers=tissuetype.Cancer(breastids)
skincancers=tissuetype.Cancer(skinids)


breastcancerset = intersect(breastcancers, subtypes1(:,1))


cancerids = arrayfun(@(x) find(strcmp(subtypes1(:,1), x)), breastcancerset);

subtypes=subtypes1(cancerids, :)

save('./Processed/subtypes.mat', 'subtypes')


load('./Processed/ccle_pathways20.mat')
load('./Processed/her2status.mat')

pathway = 'PID_ERBB2ERBB3PATHWAY'

cancerids = arrayfun(@(x) find(strcmp(filteredpathways.Cancers, x)), her2status(:,1));

pathwayid = find(strcmp(filteredpathways.Pathways, pathway))

boxplotdata = filteredpathways.data(cancerids, pathwayid)

her2status(find(strcmp(her2status(:, 2), 'Amplified')),2) = {'Her2 Amplified'}

her2status(find(strcmp(her2status(:, 2), 'Normal')),2) = {'Her2 Normal'}


h=figure;
boxplot(boxplotdata, her2status(:, 2), 'colors', clrblack)

hold on

x=zeros(length(boxplotdata),1)

aids=find(strcmp(her2status(:,2), 'Her2 Amplified'))
nids=find(strcmp(her2status(:,2), 'Her2 Normal'))
x(aids) = 2
x(nids) = 1


plot(x, boxplotdata,'.','MarkerSize', 25, 'MarkerEdgeColor', clrblue, 'MarkerFaceColor', clrblue)
 
title(strrep(pathway, '_', '\_'))

print(h, '-dpdf', sprintf('./Data/ccleannotations/%s_her2status_boxplot.pdf', pathway))

