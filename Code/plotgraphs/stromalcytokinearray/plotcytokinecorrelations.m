load('./Processed/cytokinearray.mat')
cytokinecorrelations=corr(cytokinearray.data);

h=figure;
imagesc(cytokinecorrelations)
xlabels=cytokinearray.Cytokines;
ylabels=cytokinearray.Cytokines;

xdata = 1:length(cytokinearray.Cytokines);
ydata = 1:length(cytokinearray.Cytokines);
% Set the tick locations and remove the labels 
set(gca,'XTick',xdata,'XTickLabel','') ;
set(gca,'YTick',ydata,'YTickLabel',ylabels) ;
% Define the labels 
% Estimate the location of the labels based on the position 
% of the xlabel 
hx = get(gca,'XLabel');  % Handle to xlabel 
set(hx,'Units','data'); 
pos = get(hx,'Position'); 
y = pos(2); 
% Place the new labels 
for i = 1:size(xlabels,1) 
    t(i) = text(xdata(i),y,xlabels(i,:)); 
end 
set(t,'Rotation',90,'HorizontalAlignment','right')  ;
title('Cytokine Correlations')
xlabel('Cytokines')
ylabel('Cytokines')
print(h,'-dpdf', './Normalization/cytokinescorrelation.pdf')
