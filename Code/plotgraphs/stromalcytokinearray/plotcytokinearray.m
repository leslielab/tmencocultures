load('./Processed/cytokinearrayraw.mat')

cytokinearray=cytokineraw

h2=figure(2);
imagesc(cytokinearray.data);
titlename = 'Cytokine Normalized Expression Values';

colorbar('eastoutside');
title(titlename);
xlabel('Stromal Cell Lines');
ylabel('Cytokines');
print(h2,'-dpdf', './Normalization/cytokinenormalizedppt.pdf')



titlename = 'Cytokine Raw Expression Values';
cytokinearray=cytokineexprs.data
h3=figure(3);

imagesc(cytokinearray);
colorbar('eastoutside');
title(titlename);
xlabel('Stromal Cell Lines');
ylabel('Cytokines');
print(h3,'-dpdf', './Normalization/cytokinerawppt.pdf')


load('./Processed/cytokinearraylog10.mat')

titlename = 'Cytokine Log10 Expression Values';
cytokinearray=cytokineexprs.data

h4=figure(4);
imagesc(cytokinearray);

title(titlename);
xlabel('Stromal Cell Lines');
ylabel('Cytokines');
print(h4,'-dpdf', './Normalization/cytokinelog10ppt.pdf')



Cytokines=[17 34 79]
cytokinearray=cytokineraw.data;
xlabels=cytokineraw.Stroma;
ylabels=cytokineraw.Cytokines(Cytokines,:);

h1=figure(1);
plot(cytokinearray(Cytokines,:)');

xdata = 1:size(cytokinearray, 2);
% Set the tick locations and remove the labels 
set(gca,'XTick',xdata,'XTickLabel','') ;
% Define the labels 
% Estimate the location of the labels based on the position 
% of the xlabel 
hx = get(gca,'XLabel');  % Handle to xlabel 
set(hx,'Units','data'); 
pos = get(hx,'Position'); 
y = pos(2); 
% Place the new labels 
for i = 1:size(xlabels,1) 
    t(i) = text(xdata(i),y,xlabels(i,:)); 
end 
set(t,'Rotation',90,'HorizontalAlignment','right')  ;
legend(ylabels, 'Location', 'NorthWest');
title('HGF & Other Cytokines Normalized Expression Values')
xlabel('Stromal Cell Lines')
ylabel('Cytokine Normalized Expression')
print(h1,'-dpdf', './Normalization/cytokinesoriginalnormDKK3TNFBeta.pdf')


load('./Processed/cytokinearraylog10.mat')


Cytokines=[22 97 98 103 156 272]

xlabels=cytokineexprs.Stroma;
ylabels=cytokineexprs.Cytokines(Cytokines,:);

h5=figure(5);
plot(cytokinearray(Cytokines,:)');

xdata = 1:size(cytokinearray, 2);
% Set the tick locations and remove the labels 
set(gca,'XTick',xdata,'XTickLabel','') ;
% Define the labels 
% Estimate the location of the labels based on the position 
% of the xlabel 
hx = get(gca,'XLabel');  % Handle to xlabel 
set(hx,'Units','data'); 
pos = get(hx,'Position'); 
y = pos(2); 
% Place the new labels 
for i = 1:size(xlabels,1) 
    t(i) = text(xdata(i),y,xlabels(i,:)); 
end 
set(t,'Rotation',90,'HorizontalAlignment','right')  ;
legend(ylabels, 'Location', 'NorthWest');
title('HGF & Other Cytokines Normalized Expression Values')
xlabel('Stromal Cell Lines')
ylabel('Cytokine Normalized Expression')

print(h5,'-dpdf', './Normalization/cytokineslog10ppt.pdf')

cytokinearray=quantilenorm(cytokineexprs.data);
titlename='Cytokine Expression Quantile Normalization'

h6=figure(6);
imagesc(cytokinearray);
colorbar('eastoutside');
title(titlename);
xlabel('Stromal Cell Lines');
ylabel('Cytokines');
print(h6,'-dpdf', './Normalization/cytokinequantileppt.pdf')


load('./Processed/cytokinearray1normed88.mat')
CytokineCorrelations=corr(cytokineexprs.data');

clustergram(CytokineCorrelations, 'RowLabels',  cytokineexprs.Cytokines, 'ColumnLabels', cytokineexprs.Cytokines, 'Colormap', jet)
imagesc(CytokineCorrelations)


