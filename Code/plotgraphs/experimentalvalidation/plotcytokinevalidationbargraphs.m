drugs = {'Afatinib'}
cancercelllines={'HCC827', 'HCC4006'}
cytokines={'HGF'}

dosageids={3}

drugid=1;
cancerid=1;
cytokineid=1;
dosageid=1;

clrred = [228,26,28]/255;
clrblue = [55,126,184]/255;
clrgreen = [77,175,74]/255;
clrpurple = [152,78,163]/255;
clrblack = [0 0 0];
colors = [clrgreen; clrpurple; clrblue; clrred];


for drugid = 1:size(drugs, 2)
for cancerid = 1:size(cancercelllines,2)
for cytokineid = 1:size(cytokines, 2)
for dosageid = 1:size(dosageids, 2)
   
    drug=drugs{drugid}
    cytokine=cytokines{cytokineid}
    cancercellline=cancercelllines{cancerid}
    
    dosages=importdata(sprintf('%sDosages.txt', drug))
    
    dosages=log10(dosages)
    
    baselinedosage=dosages(end)
    
    dosage=10.^dosages(dosageids{dosageid})
    

cytokinefilename = sprintf('./%s_%s_%s.txt', drug, cytokines{cytokineid}, cancercelllines{cancerid})
nocytokinefilename = sprintf('./%s_NoCytokine_%s.txt', drug, cancercelllines{cancerid})
 
    
if (exist(cytokinefilename, 'file') & exist(nocytokinefilename, 'file'))
    
    
    cytokinedata=importdata(cytokinefilename);

    cytokinebaselinedata = cytokinedata(end, :);
    
    meancytokinebaselinevalue=mean(cytokinebaselinedata');
    
    sdcytokinebaselinevalue=std(cytokinebaselinedata');

    
    
    cytokinedosagedata = cytokinedata(dosageids{dosageid}, :);
    
    meancytokinedosagevalue=mean(cytokinedosagedata');
    
    sdcytokinedosagevalue=std(cytokinedosagedata');
  
    
    
    nocytokinedata=importdata(nocytokinefilename);

    baselinedata = nocytokinedata(end, :);
    
    meanbaselinevalue=mean(baselinedata');
    
    sdbaselinevalue=std(baselinedata');

    
    
    nocytokinedosagedata = nocytokinedata(dosageids{dosageid}, :);
    
    meannocytokinedosagevalue=mean(nocytokinedosagedata');
    
    sdnocytokinedosagevalue=std(nocytokinedosagedata');

    
    
    [h p] = ttest2(cytokinedosagedata, nocytokinedosagedata, 'tail', 'right');
    
    
    
    cytokinerescuebargraphvalues=[meanbaselinevalue meancytokinebaselinevalue meannocytokinedosagevalue meancytokinedosagevalue]
    
    cytokinerescueerrorbarvalues=[sdbaselinevalue sdcytokinebaselinevalue sdnocytokinedosagevalue sdcytokinedosagevalue]
    
    
h=figure;
labels={'Baseline', sprintf('%s', cytokine), sprintf('%s %2.2f nM', drug, dosage), sprintf('%s %2.2f nM + %s', drug, dosage, cytokine)}

for ib = 1:numel(cytokinerescuebargraphvalues)
b=bar(ib,cytokinerescuebargraphvalues(ib), 0.38);
hold on;
set(b,'facecolor', colors(ib,:)) 

end
ymax=min(2, max(cytokinerescuebargraphvalues(:))+0.5);
if(p<0.5)
text(3.5,ymax-0.2,sprintf('p < %2.4f', p))
end


ax = gca;
set(ax,'XTick',1:4)
set(ax,'Xticklabels', labels);


hErrorbar=errorbar(1:4,cytokinerescuebargraphvalues,cytokinerescueerrorbarvalues,'k.')
set(hErrorbar, 'marker', 'none')
hold on;
yPos = 1;
plot(get(gca,'xlim'), [yPos yPos],  ':','Color', [50 50 50]/255); % Adapts to x limits of current axes


axis([0.375 4.625 0.0 ymax])
%legend('Oracle Neighbor','Affinity Regression','Blosum Nearest Neighbor','Nearest Neighbor')
ylabel('Relative Abundance');
title(sprintf('%s %s %2.1f nM', cancercellline, drug, dosage))


print(h, '-dpdf', sprintf('./%s_%s_%s_%2.1f.pdf',drug, cytokines{cytokineid}, cancercelllines{cancerid}, dosage))


close(h);
  
    
end

end
end
end
end
