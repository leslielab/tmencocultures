
drugs = {'Afatinib'}
cancercelllines={'HCC827','HCC4006'}
cytokines={'HGF'}
ymax={1.5, 2}


drugid=1;
cancerid=1;
cytokineid=1;

for drugid = 1:size(drugs, 2)
for cancerid = 1:size(cancercelllines,2)
for cytokineid = 1:size(cytokines, 2)
    drug=drugs{drugid}
    
    dosages=importdata(sprintf('%sDosages.txt', drug))
    
    dosages=log10(dosages)
    
    dosages=dosages(1:end-1)

filename1 = sprintf('./%s_%s_%s.txt', drug, cytokines{cytokineid}, cancercelllines{cancerid})
filename2 = sprintf('./%s_NoCytokine_%s.txt', drug, cancercelllines{cancerid})
 
    
if (exist(filename1, 'file') & exist(filename2, 'file'))
    
    
    cytokinedata1=importdata(filename1);
    
    cytokinedata1 = cytokinedata1(1:end-1, :);
    
    meancytokine1=mean(cytokinedata1');
    sdcytokine1=std(cytokinedata1');
  
    cytokinedata2=importdata(filename2);
    
    cytokinedata2 = cytokinedata2(1:end-1, :);
    
    meancytokine2=mean(cytokinedata2');
    sdcytokine2=std(cytokinedata2');
    
    
    h=figure;
    errorbar(dosages, meancytokine2,  sdcytokine2, '-o','LineWidth',2,'MarkerEdgeColor',[55 126 184]/255,'MarkerFaceColor',[55 126 184]/255, 'MarkerSize',10, 'Color', [55 126 184]/255)
    hold on
    errorbar(dosages, meancytokine1,  sdcytokine1, '-o', 'LineWidth',2,'MarkerEdgeColor',[228 26 28]/255,'MarkerFaceColor',[237 34 36]/255, 'MarkerSize',10, 'Color', [237 34 36]/255);
    hold on;
    ylim([0, ymax{cancerid}])
    
    yPos = 0.5;
    plot(get(gca,'xlim'), [yPos yPos],  ':', 'Color', [50 50 50]/255); % Adapts to x limits of current axes
    hold on
 
    yPos = 1;
    plot(get(gca,'xlim'), [yPos yPos],  ':', 'Color', [50 50 50]/255); % Adapts to x limits of current axes
    hold on;
    
    xPos = dosages(end-4);
    plot( [xPos xPos], get(gca,'ylim'), ':', 'Color', [50 50 50]/255); % Adapts to y limits of current axes
    
    hold off
    

    title(sprintf('%s treated with %s', cancercelllines{cancerid}, drug));
    xlabel('Dose (nM)');
    ylabel('Relative Abundance');
    
    legend({ drug , sprintf('%s + %s', drug, cytokines{cytokineid}) }, 'Location', 'NorthEast');
    
    ax = gca;
    xlabels= get(ax, 'XTickLabel');
    newxlabels= 10.^str2num(xlabels);
    set(ax,'XTickLabel',newxlabels);

    
    print(h, '-dpdf', sprintf('./%s_%s_%s.pdf',drug, cytokines{cytokineid}, cancercelllines{cancerid}))


    close(h);
    
end


end
end
end
