drugs = {'EGFR'}

load('../data/cancertype.mat')

threshold=.10;
clrred = [228,26,28]/255;
clrblue = [55,126,184]/255;
clrgreen = [77,175,74]/255;
clrpurple = [152,78,163]/255;
clrblack = [0 0 0];
colors = [clrgreen; clrpurple; clrblue; clrred];

subtypes={'HNSCC'}

for drugid = 1:size(drugs,2)

    drug=drugs{drugid}
   
    pvaluesraw=importdata(sprintf('%s/%s_PvaluesLeft.txt', drug, drug))
    
    string=char(pvaluesraw.textdata(1,:))
    cytokines= strsplit(string, '\t')
    cancercelllines= pvaluesraw.textdata(2:end,:)
    
    pvalues.data=pvaluesraw.data;
    pvalues.Cytokines=cytokines';
    pvalues.Cancer=cancercelllines;
    
cancerids = arrayfun(@(x) find(strcmp(cancertype.Cancer, x)), cancercelllines);
cancersubtypes=cancertype.Type(cancerids);

cytokinecellcounts=zeros(2,size(pvalues.Cytokines,1));



for subtypeid=1:size(subtypes,2)
subtypeids=find(strcmp(cancersubtypes,subtypes(subtypeid)));
    


for i = 1:size(pvalues.data, 2)
cellcount=length(find(pvalues.data(subtypeids,i)<threshold));
cytokinecellcounts(subtypeid, i)=cellcount;

end
end

keepids=find(sum(cytokinecellcounts,1)~=0);
cytokinecellcounts=cytokinecellcounts(:,keepids);
cytokinenames=pvalues.Cytokines(keepids);


[SortCellCounts sortids] = sortrows(cytokinecellcounts')

cytokinecellcounts=cytokinecellcounts(:,sortids)
cytokinenames=cytokinenames(sortids)


lungcounts=cytokinecellcounts(1,:)
lungnameids=find(lungcounts ~= 0)

lungcellcounts=[cytokinecellcounts(1,:); zeros(size(cytokinecellcounts,2),9)']
lungcellcounts=lungcellcounts(:);

h=figure;
width = 4.5;
barh(lungcellcounts, width, 'c');

maximum=length(lungcellcounts)+10;
xmin=0
xmax=max(lungcellcounts)+2.2

ylim([-10 maximum]);
xlim([xmin xmax]);

ypos=2:10:size(lungcellcounts);
xpos=zeros(1, length(ypos));

xpos(lungnameids)=cytokinecellcounts(1,lungnameids)+0.2;

text(xpos,ypos, cytokinenames)

xlabel('Cell Counts')

print(h, '-dpdf', sprintf('./%s_CellCounts_HeadandNeck.pdf',drug))


close(h);
  

end