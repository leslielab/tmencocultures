
drugs = {'BIBW2992', 'EGFR'};

load('../data/cancertype.mat')

threshold=.10;
clrred = [228,26,28]/255;
clrblue = [55,126,184]/255;
clrgreen = [77,175,74]/255;
clrpurple = [152,78,163]/255;
clrblack = [0 0 0];
colors = [clrgreen; clrpurple; clrblue; clrred];

subtypes={'NSCLC', 'Breast'}



for drugid = 1:size(drugs,2)

    drug=drugs{drugid}
   
    pvaluesraw=importdata(sprintf('%s/%s_PvaluesLeft.txt', drug, drug))
    
    string=char(pvaluesraw.textdata(1,:))
    cytokines= strsplit(string, '\t')
    cancercelllines= pvaluesraw.textdata(2:end,:)
    
    pvalues.data=pvaluesraw.data;
    pvalues.Cytokines=cytokines';
    pvalues.Cancer=cancercelllines;
    
cancerids = arrayfun(@(x) find(strcmp(cancertype.Cancer, x)), cancercelllines);
cancersubtypes=cancertype.Type(cancerids);

cytokinecellcounts=zeros(2,size(pvalues.Cytokines,1));



for subtypeid=1:size(subtypes,2)
subtypeids=find(strcmp(cancersubtypes,subtypes(subtypeid)));
    


for i = 1:size(pvalues.data, 2)
cellcount=length(find(pvalues.data(subtypeids,i)<threshold));
cytokinecellcounts(subtypeid, i)=cellcount;

end
end




keepids=find(sum(cytokinecellcounts,1)~=0);

if(length(keepids~=0))
    

cytokinecellcounts=cytokinecellcounts(:,keepids);
cytokinenames=pvalues.Cytokines(keepids);


[SortCellCounts sortids] = sortrows(cytokinecellcounts')

cytokinecellcounts=cytokinecellcounts(:,sortids)
cytokinenames=cytokinenames(sortids)

if(strcmp(drug, 'BIBW2992'))
    cytokinenames{end}='HGF'
    cytokinenames{end-1}='IL-8'
end

lungcounts=cytokinecellcounts(1,:)
breastnameids=find(lungcounts == 0)
lungnameids=find(lungcounts ~= 0)

lungcellcounts=[cytokinecellcounts(1,:); zeros(size(cytokinecellcounts,2),9)']
lungcellcounts=lungcellcounts(:);

breastcellcounts=[cytokinecellcounts(2,:); zeros(size(cytokinecellcounts,2),9)']
breastcellcounts=breastcellcounts(:)



h=figure;
width = 4.5;
barh(lungcellcounts, width, 'k');
hold on;;
barh(-breastcellcounts, width, 'r');
maximum=length(breastcellcounts)+10;
xmin=min(-breastcellcounts)-2.2
xmax=max(lungcellcounts)+2.2

ylim([-10 maximum]);
xlim([xmin xmax]);

ypos=2:10:size(lungcellcounts);
xpos=zeros(1, length(ypos));

breastnames=cytokinenames(1:size(breastnameids,2));


breastnames = strtrim(breastnames)
breastnameslength=cellfun('length', breastnames);

breastnameslength=.1*(breastnameslength) + 0.65;
breastnameslength=breastnameslength';

breastxpos = -cytokinecellcounts(2,breastnameids);

xpos(lungnameids)=cytokinecellcounts(1,lungnameids)+0.2;
xpos(breastnameids)=bsxfun(@minus, breastxpos, breastnameslength);



text(xpos,ypos, cytokinenames)

xlabel('Cell Counts')

print(h, '-dpdf', sprintf('./%s_CellCounts_Breast_NonSmallCellLung.pdf',drug))


close(h);
else
    display(sprintf('%s does not have counts', drug))
end

end