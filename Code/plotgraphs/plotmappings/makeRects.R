makeRects <- function(tfMat1R, tfMat2R, tfMat1L, tfMat2L, border1R, border2R,border1L, border2L){
  nx1R = dim(tfMat1R)[1]
  ny1R = dim(tfMat1R)[2]
  cAbove1R = expand.grid(1:nx1R,1:ny1R)[tfMat1R,]
  xl1R=cAbove1R[,1]-0.49
  yb1R=cAbove1R[,2]-0.49
  xr1R=cAbove1R[,1]+0.49
  yt1R=cAbove1R[,2]+0.49
  
  nx2R = dim(tfMat2R)[1]
  ny2R = dim(tfMat2R)[2]
  cAbove2R = expand.grid(1:nx2R,1:ny2R)[tfMat2R,]
  xl2R=cAbove2R[,1]-0.49
  yb2R=cAbove2R[,2]-0.49
  xr2R=cAbove2R[,1]+0.49
  yt2R=cAbove2R[,2]+0.49
  
  nx1L = dim(tfMat1L)[1]
  ny1L = dim(tfMat1L)[2]
  cAbove1L = expand.grid(1:nx1L,1:ny1L)[tfMat1L,]
  xl1L=cAbove1L[,1]-0.49
  yb1L=cAbove1L[,2]-0.49
  xr1L=cAbove1L[,1]+0.49
  yt1L=cAbove1L[,2]+0.49
  
  nx2L = dim(tfMat2L)[1]
  ny2L = dim(tfMat2L)[2]
  cAbove2L = expand.grid(1:nx2L,1:ny2L)[tfMat2L,]
  xl2L=cAbove2L[,1]-0.49
  yb2L=cAbove2L[,2]-0.49
  xr2L=cAbove2L[,1]+0.49
  yt2L=cAbove2L[,2]+0.49
  
  rect(xl2L,yb2L,xr2L,yt2L,border=border2L,lwd=2)
  rect(xl2R,yb2R,xr2R,yt2R,border=border2R,lwd=2)
  rect(xl1L,yb1L,xr1L,yt1L,border=border1L,lwd=2)
  rect(xl1R,yb1R,xr1R,yt1R,border=border1R,lwd=2)
}   