#install.packages("devtools")
library(gridExtra)
library(gplots)
library(devtools)
library(RColorBrewer)
source("../code/makeRects.R")
source("../code/heatmap.3.R")

N=30;


cocultures=c('BIBW2992', 'EGFR', 'Erlotinib', 'Gefitinib', 'CL-387785', 'Lapatinib', 'Canertinib', 'PLX4720', 'PD184352', 'SB590885', 'AZD6244', 'VEGFR', 'BCRABL', 'Docetaxel', 'BRAF')
thresholdl1=0.05;
thresholdl2=0.10;
thresholdr1=0.05;
thresholdr2=0.10;

for (i in 1:length(cocultures)){
  

  drug<-cocultures[i];
  dir.create(paste('./', drug, '/output/', sep=""))
  cancers<-read.table(paste(drug, "/cancers.txt", sep=""), sep="\t", header=F)
  stromas<-read.table(paste(drug, "/stromas.txt", sep=""), sep="\t", header=F)
  cytokines<-read.table(paste(drug, "/cytokines.txt", sep=""), sep="\t", header=F)
  pathways<-read.table(paste(drug, "/pathways.txt", sep=""), sep="\t", header=F)
  W<-read.table(paste(drug, "/W.txt", sep=""), sep="\t", header=F)
  
  CancerData<-read.table(paste(drug, "/CancerData.txt", sep=""), row.names = 1, sep="\t", header=T)
  StromaData<-read.table(paste(drug, "/StromaData.txt", sep=""), row.names = 1, sep="\t", header=T)
  cancertype<-read.table("../data/cancertype.txt", sep="\t", header=F)
  stromatype<-read.table("../data/stromatype.txt", sep="\t", header=F)
  subtype<-read.table("../data/subtype.txt", sep="\t", header=F)
  
  cancerannotation=as.matrix(apply(cancers,1, function(x){cancertype[which(cancertype[,1] == x),3]}))
  cancerlegend=as.matrix(apply(cancers,1, function(x){cancertype[which(cancertype[,1] == x),2]}))
  cancerlegend=as.matrix(unique(cancerlegend));
  cancerlegendfill=as.matrix(apply(cancerlegend,1, function(x){cancertype[which(cancertype[,2] == x)[1],3]}))
  stromaannotation=as.matrix(apply(stromas,1, function(x){stromatype[which(stromatype[,1] == x),3]}))
  stromaannotation<-as.matrix(t(stromaannotation))
  rownames(stromaannotation) <- c("Stroma Subtype")
  
  if(drug=="Erlotinib"| drug=="BIBW2992" | drug=="CL-387785"| drug=="Canertinib"){
    subtypeannotation=as.matrix(apply(cancers,1, function(x){subtype[which(subtype[,1] == x),3]}))
    cancerannotation = cbind(cancerannotation, subtypeannotation);
    cancerannotation<-as.matrix(t(cancerannotation))
    rownames(cancerannotation) <- c("Cancer Subtype", "Her2 Positive Status")
    
    cancerlegend= c(cancerlegend,"","Her2 Positive","Her2 Negative")
    cancerlegendfill=c(cancerlegendfill,"white","purple","green")
  }else{
    cancerannotation<-as.matrix(t(cancerannotation))
    rownames(cancerannotation) <- c("Cancer Subtype")
  }
  
  cancers<-as.matrix(cancers)
  stromas<-as.matrix(stromas)
  cytokines<-as.matrix(cytokines)
  pathways<-as.matrix(pathways)
  #Colors including Steve's palette
  #my_palette <- colorRampPalette(c("#405889","#778aac","#b4bece", "#E4F5F2", "#caa2aa","#a75060","#8c2528"))(n = 299)
  #my_palette <- colorRampPalette(c("#06E3E3","#1AF4D0","orange", "#E4F5F2", "#E306E3","red", "#E306E3"))(n = 299
  #my_palette <- colorRampPalette(c("#06E3E3","#1AF4D0", "#E4F5F2","#EA95FF", "#FF06E3"))(n = 100)
  jet.colors <- colorRampPalette(c("#00007F", "blue", "#007FFF", "cyan", "#7FFF7F", "yellow", "#FF7F00", "red", "#7F0000"))
  my_palette <- jet.colors(1000)[c(seq(1,125, length.out=810),seq(125,250, length.out=63), seq(250,375, length.out=63),seq(375,500, length.out=63), seq(500,625, length.out=63), seq(625,750, length.out=63),seq(750, 875, length.out=63), seq(875,1000,length.out=810))]
  
  
  # rownames(W)<-pathways
  #  colnames(W)<-cytokines
  #  W<-as.matrix(W)
  #  hRowv <- hclust(dist(W))
  #  hColv <- hclust(dist(t(W)))
  #  Rowv <- as.dendrogram(hRowv)
  #  Colv <- as.dendrogram(hColv)
  
  
  
  # PvalueTable<-read.table(paste(drug, '/', drug, "_InteractionPvalueTable.txt", sep=""), sep="\t", header=T)
  
  #WPvalues=NULL;
  
  # for (pid in 1:dim(PvalueTable)[1]){
  #   if(PvalueTable[pid,1]==1){
  
  #    WPvalues=c(WPvalues, PvalueTable[pid,5]);
  #  }else{
  #   WPvalues=c(WPvalues, PvalueTable[pid,6]);
  #}    
  #}
  
  #  WPvaluesRight=p.adjust(PvalueTable[,7], method = "BY");;
  #  WPvaluesLeft=p.adjust(PvalueTable[,8], method = "BY");;
  #  numpathways=length(pathways);
  # numcytokines=length(cytokines);
  #  WPvaluesRight<-matrix(WPvaluesRight, nrow=numpathways, ncol=numcytokines)
  #  WPvaluesLeft<-matrix(WPvaluesLeft, nrow=numpathways, ncol=numcytokines)
  #  WPvalues<-matrix(WPvalues, nrow=numpathways, ncol=numcytokines)
  
  # rownames(WPvalues)<-pathways
  #  colnames(WPvalues)<-cytokines
  # Pvalues<-as.matrix(WPvalues)
  
  #ids=which(WPvalues < threshold2, arr.ind=TRUE)
  # Wpnames = rownames(WPvalues)[ids[,1]]
  #  Wcnames = colnames(WPvalues)[ids[,2]]
  #  Wpvalues = WPvalues[ids]
  # Interactions=as.data.frame(cbind(Wcnames, Wpnames, Wpvalues))
  #colnames(Interactions)<-c("Cytokines", "Pathways", "P-value")
  #  write.table(Interactions, file=paste(drug, "/output/", drug, "_Cytokine_Pathway_Interactions.txt", sep=""), quote=F, col.names=F, row.names=F, sep="\t")
  
  
  # rownames(WPvaluesRight)<-pathways
  #  colnames(WPvaluesRight)<-cytokines
  # PvaluesRight<-as.matrix(WPvaluesRight)
  
  
  #  WPvalsRight=WPvaluesRight[rev(hRowv$order), rev(hColv$order)]
  #  thresholdmatR1 <- WPvalsRight<threshold1 #matrix used for the example instead of the p value matrix
  # thresholdmatR1 <- apply(thresholdmatR1, 2, rev) 
  #thresholdmatR1 <- t(thresholdmatR1)
  
  #  thresholdmatR2 <- WPvalsRight<threshold2 #matrix used for the example instead of the p value matrix
  #  thresholdmatR2 <- apply(thresholdmatR2, 2, rev) 
  #  thresholdmatR2 <- t(thresholdmatR2)
  
  
  # rownames(WPvaluesLeft)<-pathways
  #  colnames(WPvaluesLeft)<-cytokines
  # PvaluesLeft<-as.matrix(WPvaluesLeft)
  
  
  #WPvalsLeft=WPvaluesLeft[rev(hRowv$order), rev(hColv$order)]
  #thresholdmatL1 <- WPvalsLeft<threshold1 #matrix used for the example instead of the p value matrix
  #thresholdmatL1 <- apply(thresholdmatL1, 2, rev) 
  #thresholdmatL1 <- t(thresholdmatL1)  
  
  
  #  WPvalsLeft=WPvaluesLeft[rev(hRowv$order), rev(hColv$order)]
  #thresholdmatL2 <- WPvalsLeft<threshold2 #matrix used for the example instead of the p value matrix
  #thresholdmatL2 <- apply(thresholdmatL2, 2, rev) 
  #  thresholdmatL2 <- t(thresholdmatL2)
  
  # WPvals=WPvalues[rev(hRowv$order), rev(hColv$order)]
  #WlogPvals=-log10(WPvals)
  #WlogPvals
  
  
  #W=W[rev(hRowv$order), rev(hColv$order)]
  
  #pdf(paste(drug, "/output/",drug,"_PvaluesfortheInteractions.pdf", sep=""))
  #heatmap.3(WlogPvals,Rowv = FALSE, Colv=FALSE, main = drug, cexCol=0.35,cexRow=0.35,trace="none",dendrogram= "none", margins =c(10,10), col=my_palette, symm=F,symkey=F,symbreaks=T, add.expr = {makeRects(thresholdmatR1,thresholdmatR2,thresholdmatL1,thresholdmatL2, "orange", "orange", "deepskyblue1", "deepskyblue1")}) #add the borders
  #dev.off()
  
  
  #  pdf(paste(drug, "/output/",drug,"_InteractionPvalues.pdf", sep=""))
  #  heatmap.3(W, Rowv=FALSE, Colv=FALSE,dendrogram= "none", main = drug, cexCol=0.35,cexRow=0.35,trace="none",col=my_palette, symm=F,symkey=F,symbreaks=T, margins=c(10,10), add.expr = {makeRects(thresholdmatR1,thresholdmatR2,thresholdmatL1,thresholdmatL2, "orange", "orange", "deepskyblue1", "deepskyblue1")}) #add the borders
  #  dev.off()
  
  
  
  A<-read.table(paste(drug, "/A.txt", sep=""), sep="\t", header=F)
  rownames(A)<-cancers
  colnames(A)<-cytokines
  
  
  ProjectionValues<-as.matrix(A)
  
  
  hRowv <- hclust(dist(ProjectionValues))
  hColv <- hclust(dist(t(ProjectionValues)))
  Rowv <- as.dendrogram(hRowv)
  Colv <- as.dendrogram(hColv)
  
  
  
  #ProjectionValues[hRowv$order, hColv$order]
  
 
  
  
  PvalueRightp<-read.table(paste(drug, '/',"my_ccm_left_10000.csv", sep=""), sep=",", header=F)
  
  PvalueLeftp<-read.table(paste(drug, '/',"my_ccm_right_10000.csv", sep=""), sep=",", header=F)
  
  
  
  PvalueLeft<-as.matrix(PvalueLeftp)
  PvalueLeft[which(PvalueLeft==0)]=1
  
  
  PvalueLeft=PvalueLeft/10000
  
  
  PvalueRight<-as.matrix(PvalueRightp)
  PvalueRight[which(PvalueRight==0)]=1
  
  
  PvalueRight=PvalueRight/10000
  
  
  PvaluesRight=apply(PvalueRight, 1, function(x){p.adjust(x, method = "BH")})
  
  PvaluesLeft=apply(PvalueLeft, 1, function(x){p.adjust(x, method = "BH")})
  
  PvaluesRight=t(PvaluesRight)
  
  PvaluesLeft=t(PvaluesLeft)
  
  
  Pvalues=NULL;
  for (pid in 1:length(PvaluesRight)){
    if(PvaluesLeft[pid]<PvaluesRight[pid]){
      Pvalues=c(Pvalues, PvaluesLeft[pid]);
    }else{
      Pvalues=c(Pvalues, PvaluesRight[pid]);
    }    
  }
  
  
  numcancers=length(cancers);
  numcytokines=length(cytokines);
  PvaluesRight<-matrix(PvaluesRight, nrow=numcancers, ncol=numcytokines)
  PvaluesLeft<-matrix(PvaluesLeft, nrow=numcancers, ncol=numcytokines)
  Pvalues<-matrix(Pvalues, nrow=numcancers, ncol=numcytokines)
  
  
  rownames(Pvalues)<-cancers
  colnames(Pvalues)<-cytokines
  Pvalues<-as.matrix(Pvalues)
  

  
  
  ids=which(Pvalues < thresholdl2, arr.ind=TRUE)
  rnames = rownames(Pvalues)[ids[,1]]
  cnames = colnames(Pvalues)[ids[,2]]
  pvalues = Pvalues[ids]
  ProjectionInteractions=as.data.frame(cbind(cnames, rnames, pvalues))
  colnames(ProjectionInteractions)<-c("Cytokines", "Cancer Cell Line", "P-value")
  write.table(ProjectionInteractions, file=paste(drug,"/output/", drug, "_Cytokine_ProjectionInteractions.txt", sep=""), quote=F, col.names=F, row.names=F, sep="\t")
  
  #pdf(paste(drug, "/output/",drug,"_Cytokine_ProjectionInteractions.pdf", sep=""))
  #grid.arrange(tableGrob(ProjectionInteractions, core.just='left', gpar.coretext = gpar(col = "black", cex = 0.85), gpar.coltext = gpar(col = "black", cex = 0.85, fontface = "bold"), gpar.rowtext = gpar(col = "black", cex = 0.85, fontface = "italic")), main = paste(drug, " Projection Interactions", sep=""));
  #dev.off()
  
  
  rownames(PvaluesRight)<-cancers
  colnames(PvaluesRight)<-cytokines
  PvaluesRight<-as.matrix(PvaluesRight)
  
  
  PvalsRight=PvaluesRight[rev(hRowv$order), hColv$order]
  thresholdmatR1 <- PvalsRight<thresholdr1 #matrix used for the example instead of the p value matrix
  thresholdmatR1 <- apply(thresholdmatR1, 2, rev) 
  thresholdmatR1 <- t(thresholdmatR1)
  
  thresholdmatR2 <- PvalsRight<thresholdr2 #matrix used for the example instead of the p value matrix
  thresholdmatR2 <- apply(thresholdmatR2, 2, rev) 
  thresholdmatR2 <- t(thresholdmatR2)
  
  
  rownames(PvaluesLeft)<-cancers
  colnames(PvaluesLeft)<-cytokines
  PvaluesLeft<-as.matrix(PvaluesLeft)
  
  
  PvalsLeft=PvaluesLeft[rev(hRowv$order), hColv$order]
  thresholdmatL1 <- PvalsLeft<thresholdl1 #matrix used for the example instead of the p value matrix
  thresholdmatL1 <- apply(thresholdmatL1, 2, rev) 
  thresholdmatL1 <- t(thresholdmatL1)
  
  
  
  PvalsLeft=PvaluesLeft[rev(hRowv$order), hColv$order]
  thresholdmatL2 <- PvalsLeft<thresholdl2 #matrix used for the example instead of the p value matrix
  thresholdmatL2 <- apply(thresholdmatL2, 2, rev) 
  thresholdmatL2 <- t(thresholdmatL2)
  
  
  pdf(paste(drug, "/output/",drug,"_LeftProjectionPvalues.pdf", sep=""))
  heatmap.3(ProjectionValues, Rowv=Rowv, Colv=Colv, main = drug, cexCol=0.48,cexRow=0.75,trace="none",col=my_palette, symm=F,symkey=F,symbreaks=T, RowSideColors=cancerannotation, cexRowSideColors=1, RowSideColorsSize=1.5, margins=c(15,15), add.expr = {makeRects(thresholdmatR1,thresholdmatR2,thresholdmatL1,thresholdmatL2, "#ffc300", "orange", "deepskyblue1", "deepskyblue1")}) #add the borders
  
  legend("topright",legend=cancerlegend,
         fill=cancerlegendfill, border=FALSE, bty="n", y.intersp = 0.7, cex=0.7)
  dev.off()
  
  
  write.table(PvaluesRight, file=sprintf("%s/%s_PvaluesLeft.txt",drug,drug), quote=FALSE, sep="\t")
  
}