
function writearmtltofile(output, rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway)



mkdir(sprintf('./Results/Pathways/rescue%dcyto%dnorm%dpathway%d/%s/', rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway, output));

input = sprintf('models/Pathway_WholeModels_JointGroup/%s/models_rescue%d_cyto%d_norm%d_pathway%d_wholemodel.mat', output,  rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway)
load(input);


%---------------------------------The Projections--------------------------------------

Y = model.Y;
W = model.Wcommon;
C = model.C;
S = model.S;



w = reshape(W,size(C.data,2),size(S.data,2));

A=C.data*w;
B=w*S.data';
CancerData=[' ' C.Cancers'; C.Pathways num2cell(C.data)];
StromaData=[' ' S.Stroma'; S.Cytokines num2cell(S.data)];

%Then take away the zero columns of A and B
dlmwrite(sprintf('./Results/Pathways/rescue%dcyto%dnorm%dpathway%d/%s/A.txt', rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway,output),A,'\t')
dlmwrite(sprintf('./Results/Pathways/rescue%dcyto%dnorm%dpathway%d/%s/B.txt', rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway,output),B,'\t')
dlmwrite(sprintf('./Results/Pathways/rescue%dcyto%dnorm%dpathway%d/%s/W.txt', rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway,output),w,'\t')

dlmcell(sprintf('./Results/Pathways/rescue%dcyto%dnorm%dpathway%d/%s/cytokines.txt', rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway,output),S.Cytokines,'\t')
dlmcell(sprintf('./Results/Pathways/rescue%dcyto%dnorm%dpathway%d/%s/stromas.txt', rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway,output),S.Stroma,'\t')
dlmcell(sprintf('./Results/Pathways/rescue%dcyto%dnorm%dpathway%d/%s/cancers.txt', rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway,output),C.Cancers,'\t')
dlmcell(sprintf('./Results/Pathways/rescue%dcyto%dnorm%dpathway%d/%s/pathways.txt', rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway,output),C.Pathways,'\t')

dlmcell(sprintf('./Results/Pathways/rescue%dcyto%dnorm%dpathway%d/%s/CancerData.txt', rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway,output),CancerData,'\t')
dlmcell(sprintf('./Results/Pathways/rescue%dcyto%dnorm%dpathway%d/%s/StromaData.txt', rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway,output),StromaData,'\t')



%---------------------------------The Rescue Matrix--------------------------------------




for rid = 1:size(Y.rescue, 1)

drug = Y.description{rid}.drug ;
dosage = Y.description{rid}.dosage;
description = {drug dosage 'V3' 'V4' 'V5' 'V6' 'V7' 'V8' 'V9' 'V10' 'V11' 'V12' 'V13' 'V14' 'V15' 'V16'  'V17' 'V18'};    
dlmcell(sprintf('./Results/Pathways/rescue%dcyto%dnorm%dpathway%d/%s/%s_%2.2f_Rescue.txt', rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway,output, drug, dosage), description, '\t')
rescue = num2cell(Y.rescue{rid});
dlmcell(sprintf('./Results/Pathways/rescue%dcyto%dnorm%dpathway%d/%s/%s_%2.2f_Rescue.txt', rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway,output, drug, dosage), rescue, '\t', '-a')

end   





end