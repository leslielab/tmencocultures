function [W, ccle_pathways, cytokineexpressions, Y]=multitaskaffinityregression(drug_group,  rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway, lambda, rho1, rho2)


input = sprintf('./Processed/coculturegroupsrescue%dcyto%dnorm%dpathway%d/%s_drug_group.mat',  rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway, drug_group);


datasource = 'TMEN';
fprintf('running %s\n', datasource);
nfolds = 10;


load(input);

% Parameter search

%lambdas = [0 0.001 0.01 0.1 1 10 50 100];
%rho_L2s = [0 0.001 0.01 0.1 1 10 50 100];


%[L1 L2] = ndgrid(lambdas, rho_L2s);
%params = [L1(:) L2(:)];
number_tasks=length(data.X);
t=number_tasks;

R = eye(t) - ones(t)/t;

%rng('default');     % reset random generator.Available from Matlab 2011.
opts.init = 0;      % guess start point from data. 
opts.tFlag = 1;     % terminate after relative objective value does not changes much.
opts.tol = 10^-5;   % tolerance. 
opts.maxIter = 500; % maximum iteration number of optimization.



opts.rho_L2=rho2;


%%% There can be other choices of the R (for different structures).
%R = eye(task_num); % ridge penalty.
%R = zeros(t,t-1);H(1:(t+1):end)=1;H(2:(t+1):end)=-1; % order structure
%R = eye (task_num) - ones (task_num) / task_num;  %regularized MTL penalty

[W_est funcVal] = Least_SRMTL(data.X, data.Y, R, lambda, rho1, opts);

%plot(funcVal)

W=(lambda/(rho2+lambda))*(1/t)*mean(W_est,2);

%W_t=W_est(:,4)-W;

  model.data = data;
  model.W = W_est;
  model.Wcommon = W;
  model.C=ccle_pathways;
  model.S=cytokineexpressions;
  model.Y= Y;
  model.lambda = lambda;
  model.opts = opts;
  model.funVal = funcVal;
  
  
  
   mkdir(sprintf('models/Pathway_WholeModels_JointGroup/%s/', drug_group));

   filename = sprintf('models/Pathway_WholeModels_JointGroup/%s/models_rescue%d_cyto%d_norm%d_pathway%d_wholemodel.mat', drug_group,  rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway);
   parsavewholemodel(filename, model, lambda, rho1, rho2);
   fprintf('saved %s \n', filename);
    
end



