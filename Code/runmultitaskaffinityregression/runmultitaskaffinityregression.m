
drugs = {'Erlotinib', 'BIBW2992', 'Gefitinib', 'CL-387785', 'Lapatinib', 'Canertinib', 'PLX4720', 'PD184352', 'SB590885', 'AZD6244', 'VEGFR', 'BCRABL', 'Docetaxel', 'EGFR', 'BRAF'}
load('./Results/plots/ARMTLvsLeastRPerformance/ARMTLvsLeastRPerformance.mat')
lambdas=ParametersARMTL(:,1);
rhos=ParametersARMTL(:,2);

% Data Process
rho=0;
rescuenormalization=2;
cytokinearraynumber=1;
cytokinenormalization=88;
pathway=20;


for i = 1%:size(drugs, 2)
   [W, ccle_pathways, cytokineexpressions, Y]= multitaskaffinityregression(drugs{i},  rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway, lambdas(i), rho, rhos(i));
    writearmtltofile(drugs{i}, rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway)
end