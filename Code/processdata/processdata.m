function processdata(drugs, output, rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway, dosage_setting, type)

%Load the data into matlab
%Just load the rescue data: filtered
load('Processed/filteredrescue.mat');
load('Processed/cancerpathways.mat');
load('Processed/cytokinearray.mat');
load('Processed/cancertype.mat');
load('Processed/activedrugs.mat')

%We subset the data set by the drugs
number_drugs= size(drugs,1);
cclecancers=cancerpathways.Cancers;
cytokinearraystromas=cytokinearray.Stroma;

drugdosageids=[];

%Process monoculture and coculture

monoculture=filteredrescue.Effect_Without_Stroma;

coculture=filteredrescue.Effect_With_Stroma;


pseudocount=0.6177;

logRescue=log2(coculture+pseudocount)-log2(monoculture+pseudocount);


logRescue=zscore(logRescue);


filteredrescue.logRescue=logRescue;


for dr = 1:number_drugs
    
drug =  drugs{dr};
    
drugids = find(strcmp(filteredrescue.Drug, drug));    
    
drugrescue = filteredrescue.textdata(drugids, :);


activedrugids = find(strcmp(activedrugs.Drug, drug));    


activedosages = unique(activedrugs.uM(activedrugids, :));


filtereddrugids = find(strcmp(filteredrescue.Drug, drug));    


filtereddosages = unique(filteredrescue.uM(filtereddrugids, :));


dosages=intersect(activedosages, filtereddosages);


num_dosages=size(dosages, 1);


if(dosage_setting=='full')
    
    start_dosage=1;
    
else
start_dosage=ceil(num_dosages/2);
end


for do = start_dosage:num_dosages
dosage=dosages(do);
drugids = find(strcmp(filteredrescue.Drug, drug) & filteredrescue.uM == dosage);
drugdosageids=[drugdosageids drugids'];
end

end



druggrouprescue = filterrescues(filteredrescue, drugdosageids);

stromas = unique(druggrouprescue.Stroma);

cancers = unique(druggrouprescue.Cancer);

cancers=intersect(cclecancers,cancers);


if (~strcmp(type, 'all'))
    ids = find(strcmp(cancertype.Type, type));
    thiscancertype = cancertype.Cancer(ids);
    cancers = intersect(cancers, thiscancertype)
    
end


number_stromas=size(stromas,1);
number_cancers=size(cancers,1);




Y.rescue=[];
Y.description=[];
drugindex=1;
for dr = 1:number_drugs

drug=drugs{dr}
    
drugids = find(strcmp(druggrouprescue.Drug, drug));    
    
drugrescue = druggrouprescue.textdata(drugids, :);

dosages = unique(druggrouprescue.uM(drugids, :));

num_dosages=size(dosages, 1);



for do = 1:num_dosages
dosage=dosages(do);
drugdosageids = find(strcmp(druggrouprescue.Drug, drug) & druggrouprescue.uM == dosage);




drugdosagerescue = filterrescues(druggrouprescue, drugdosageids);

%Create the rescue matrix for the drug dosage coculture system

rescue_matrix = zeros(number_cancers, number_stromas);

for cancer_id = 1:number_cancers
   
    
    for stroma_id = 1:number_stromas
  
        
        coculture_ids = find(strcmp(drugdosagerescue.Cancer, cancers(cancer_id)) & strcmp(drugdosagerescue.Stroma, stromas(stroma_id)));
    

       if(size(coculture_ids, 1) == 1)
           
           rescue_matrix(cancer_id, stroma_id) = drugdosagerescue.logRescue(coculture_ids);
           
           
           
           
       else
           
           rescue_matrix(cancer_id, stroma_id) = nan;

       end
        
        
    end
end



if(rescuenormalization == 2)
rescue_matrix = reshape(rescue_matrix(:), number_cancers, number_stromas);

end


numberrescuevalues=sum(~isnan(rescue_matrix(:)))

if(numberrescuevalues>50)
    

Y.rescue{drugindex,1}=rescue_matrix;
Y.description{drugindex,1}.drug=drug;
Y.description{drugindex,1}.dosage=dosage;


drugindex=drugindex+1;

elseif(numberrescuevalues<50)

    fprintf('%s %g has only %d values', drug, dosage, numberrescuevalues);

end


end
end


stromaids = arrayfun(@(x) find(strcmp(cytokinearray.Stroma, x)), stromas);

cytokinearray.data=cytokinearray.data(stromaids, :);
cytokinearray.Stroma=cytokinearray.Stroma(stromaids);


%Then load the nmf ccle representation


cancerids = arrayfun(@(x) find(strcmp(cancerpathways.Cancers, x)), cancers);


cancerpathways.data= cancerpathways.data(cancerids,:);
cancerpathways.Cancers= cancerpathways.Cancers(cancerids);
cancerpathways.Pathways= cancerpathways.Pathways;


%Create data sets

        A = cancerpathways.data;
        B = cytokinearray.data;
        X = kron(B, A);
       
        
        number_tasks=size(Y.rescue,1);
        Y.holdout = cell(1, number_tasks);
        data.Y=cell(1,number_tasks);
        data.X=cell(1,number_tasks);
        data.holdout=cell(1,number_tasks);
        data.info=cell(1,number_tasks);
        
for t = 1:number_tasks
         
          
        %convert Y into a vector
        Y_t=Y.rescue{t}(:);
        %Get rid of the nans
        idx=find(~isnan(Y_t));
        Y_t=Y_t(idx);

        data.Y{t} = Y_t;

        %Get the corresponding rows of the X 
        X_t=X(idx,:);
        
        data.X{t} = X_t;
         
        
        %Get the hold out set
        stream = RandStream.getGlobalStream;
        reset(stream,100);
        data.holdout{t}=randperm(length(Y_t))';
        
        
        %information drug and dosage
        data.info{t}=Y.description{t};
       
        Y.holdout{t}= idx;
end
        




save(sprintf('./Processed/coculturegroupsrescue%dcyto%dnorm%dpathway%d/%s_drug_group.mat',  rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway, output), 'data', 'druggrouprescue', 'Y',  'stromas', 'cancers', 'cytokinearray', 'cancerpathways');
     
end