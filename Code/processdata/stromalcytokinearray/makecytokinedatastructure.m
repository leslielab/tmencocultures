
function cytokineexpression = makecytokinedatastructure(cytokinearray)

number_cytokines = size(cytokinearray.textdata, 1);
number_stroma = size(cytokinearray.textdata, 2);
cytokines = cytokinearray.textdata(2:number_cytokines,1);
stroma = cytokinearray.textdata(1,2:number_stroma);

[stroma_sorted sids] = sort(stroma);
cytokineexpression.data = cytokinearray.data(:, sids);
cytokineexpression.Stroma = stroma_sorted';
cytokineexpression.Cytokines = cytokines;

end

