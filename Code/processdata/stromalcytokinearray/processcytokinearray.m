cytokineexpression=importdata('Data/cytokine1.txt');

cytokineraw=makecytokinedatastructure(cytokineexpression);


save('./Processed/cytokinearrayraw.mat', 'cytokineraw')


minimumvalue=ceil(abs(min(min(cytokineraw.data))))+1

cytokineraw.data= cytokineraw.data+minimumvalue;

cytokineexprs.data=log10(cytokineraw.data)

save('./Processed/cytokinearraylog10.mat', 'cytokineexprs')


% Perform a zscore
cytokinezscore.data = zscore(cytokineexprs.data);
titlename = 'Cytokine Zscore Expression Values';


mkdir('./Normalization/')

h1=figure(1);
imagesc(cytokinezscore.data);
colorbar('eastoutside');
title(titlename);
xlabel('Stromal Cell Lines');
ylabel('Cytokines');
print(h1,'-dpdf', './Normalization/cytokinezscoreppt.pdf')


%Cut-off
ix = find(max(cytokinezscore.data, [], 2) > 0.75);
cytokines=cytokineexprs.Cytokines(ix,:)
titlename = 'Cytokine Normalized Expression Values';

cytokineexpression=cytokinezscore.data(ix,:);
h2=figure(2);
imagesc(cytokineexpression);
ydata = 1:size(cytokineexpression, 1)
ylabels = cytokines;
set(gca, 'Ytick', ydata, 'YtickLabel', ylabels, 'FontSize',9)

xdata = 1:size(cytokineexpression, 2);
% Set the tick locations and remove the labels 
set(gca,'XTick',xdata,'XTickLabel','') ;
% Define the labels 
xlabels = cytokineexprs.Stroma;
% Estimate the location of the labels based on the position 
% of the xlabel 
hx = get(gca,'XLabel');  % Handle to xlabel 
set(hx,'Units','data'); 
pos = get(hx,'Position'); 
y = pos(2); 
% Place the new labels 
for i = 1:size(xlabels,1) 
    t(i) = text(xdata(i),y,xlabels(i,:)); 
end 
set(t,'Rotation',90,'HorizontalAlignment','right');
colorbar('eastoutside');
title(titlename);
xlabel('Stromal Cell Lines');
ylabel('Cytokines');

print(h2,'-dpdf', './Normalization/cytokinezscorecutppt.pdf')


h3=figure(3);
Cytokines=[7 17 42]
plot(cytokineexpression(Cytokines,:)');

xdata = 1:size(cytokineexpression(Cytokines,:), 2);
% Set the tick locations and remove the labels 
set(gca,'XTick',xdata,'XTickLabel','') ;
% Define the labels 
% Estimate the location of the labels based on the position 
% of the xlabel 
hx = get(gca,'XLabel');  % Handle to xlabel 
set(hx,'Units','data'); 
pos = get(hx,'Position'); 
y = pos(2); 
% Place the new labels 
for i = 1:size(xlabels,1) 
    t(i) = text(xdata(i),y,xlabels(i,:)); 
end 
set(t,'Rotation',90,'HorizontalAlignment','right')  ;
legend(ylabels(Cytokines, :), 'Location', 'NorthWest');
title('DKK-3 HGF and TNF-Beta')
xlabel('Stromal Cell Lines')
ylabel('Cytokine Normalized Expression')

print(h3,'-dpdf', './Normalization/cytokineszscorecutpptDKK3TNFbeta.pdf')



h4=figure(4);
hist(cytokinezscore.data,100)
title('Cytokines Normalized Expression Values')
xlabel('Normalized Expression Values')
ylabel('Count')
print(h1,'-dpdf', './Normalization/cytokinenormhistogram.pdf')


a = cytokinezscore.data;
h5=figure(5), qqplot(a(:))
print(h5,'-dpdf', './Normalization/cytokinezscoreqqplot.pdf')


b = (cytokineexprs.data);
h6=figure(6), qqplot(b(:))
print(h6,'-dpdf', './Normalization/cytokinelog10qqplot.pdf')


b = quantilenorm(zscore(cytokineexprs.data));
h7=figure(7), qqplot(b(:))
print(h7,'-dpdf', './Normalization/cytokinequantileqqplot2.pdf')


maxzscore=max(cytokinezscore.data, [], 2);
h8=figure(8), plot(maxzscore);
title('Cytokine Expression Cutoff')
xlabel('Index')
ylabel('Normalized Cytokine Expression')
print(h8,'-dpdf', './Normalization/cytokinecutoffmaxrow.pdf')

maxzscoresorted=sort(maxzscore);
h9=figure(9), plot([0 300],[0.75 0.75], 'r-.');
hold on;
plot(maxzscoresorted);
title('Cytokine Expression Cutoff')
xlabel('Sorted Index')
ylabel('Normalized Cytokine Expression')
print(h9,'-dpdf', './Normalization/cytokinecutoffmaxrowsort.pdf')


cytokinearray.data=cytokineexpression';
cytokinearray.Stroma = cytokineexprs.Stroma;
cytokinearray.Cytokines = cytokines;


save('./Processed/cytokinearray.mat', 'cytokinearray')
