

% Intial importing ccle expression data into matlab
CCLE=importdata('Data/exportccletomatlab/ccleexpressiondata.txt');
cancers=textread('Data/exportccletomatlab/cclecancers.txt',  '%s',  'delimiter', '\t');
genes=textread('Data/exportccletomatlab/cclegenes.txt',  '%s',  'delimiter', '\t');
% Reorganize the data into a data structure
ccle.data=CCLE;
ccle.Cancers=cancers;
ccle.Genes=genes;
% Save the data
save('./Processed/ccledata.mat','ccle')


% Import pathways
curatedpathways=importdata('./Data/pathways.txt');

% Data Processing:
% Load ccle processed data
load('./Processed/ccledata.mat')

% Load Pathways
pathways=importdata('Data/c2.all.v4.0.symbols.txt');

% Process the pathways data
cancerpathways=cellfun(@(x) strsplit(x, '\t'), pathways, 'UniformOutput', false);

cancerpathwaysempty=cellfun(@(x) isempty(x{end}), cancerpathways);

cancerpathwaysfiltered=cellfun(@(x) x(1:(end-1)), cancerpathways,'UniformOutput', false );

cancerpathwaysempty=cellfun(@(x) isempty(x{end}), cancerpathwaysfiltered);

pathways=cancerpathwaysfiltered;

% Save the cancer pathways that have been processed:
save('./Processed/pathwaylist.mat','pathways')


% Load the processed cancer pathways
load('./Processed/pathwaylist.mat')
load('./Processed/ccledata.mat')

cclegenes=ccle.Genes;
ids=[];

pid=1


% Filter out those pathways with genes that are missing more than 10%
for pid=1:length(pathways)

pathwaygenes=pathways{pid}(2:end);

missinggenes=setdiff(pathwaygenes, cclegenes);

percentmissinggenes=length(missinggenes)/length(pathwaygenes);

if(percentmissinggenes>.10)
    
    ids=[ids pid];
    
    
    display(pathways{pid}(1));
    
end

end

filteredpathways = pathways;

filteredpathways(ids)=[];


save('./Processed/filteredcancerpathways.mat','filteredpathways')


% Load the cancer data

load('./Processed/filteredcancerpathways.mat')

load('./Processed/ccledata.mat')


% Filter out the missing genes from the pathways

pathways.data = cell(length(filteredpathways),1);
pathways.Names = cell(length(filteredpathways),1);

cclegenes=ccle.Genes;


for pid=1:length(pathways.data)
pathways.Names(pid)=filteredpathways{pid}(1);
pathwaygenes=filteredpathways{pid}(2:end);


intersectiongenes=intersect(pathwaygenes, cclegenes);
pathways.data{pid}=intersectiongenes;

missinggenes=setdiff(pathwaygenes, cclegenes);

percentmissinggenes=length(missinggenes)/length(pathwaygenes);

if(percentmissinggenes>.10)
    
    
    display(filteredpathways{pid}(1));
    
end

end

% Save pathways

save('./Processed/pathways.mat','pathways')




% Filter for Curated Pathways


% Filter by my pathways in the name then unit normalize
load('./Processed/pathways.mat')
load('./Processed/ccledata.mat')
load('./Processed/curatedpathways.mat')


ids=arrayfun(@(x) find(strcmp(pathways.Names, x)), curatedpathways);


pathways.data=pathways.data(ids,:);

pathways.Names=pathways.Names(ids,:);

save('./Processed/pathwayscurated.mat', 'pathways')




% Write to file pathway memberships
load('./Processed/pathwayscurated.mat')

for pid = 1:size(pathways.Names, 1)
    
pathway = [pathways.Names{pid} pathways.data{pid}'];
dlmcell('./Data/pathwaymembers.txt', pathway, '\t', '-a')

end   



% Load pathways from saved mat file
% Then get the cancer representation as pathways.

load('./Processed/pathwayscurated.mat')
load('./Processed/ccledata.mat')
load('./Processed/cancertype.mat');


mkdir('Results/plots/PathwayPlots/')

cancer_pathway_values=zeros(length(pathways.data), length(ccle.Cancers));

genes_pathway_values=zeros(length(pathways.data), length(ccle.Genes));


cancers=cancertype.Cancer;
cancerids= [];

for i = 1:size(cancers, 1)
ids = find(strcmp(ccle.Cancers, cancers(i)));
cancerids = [cancerids; ids];
end

colors = {'r','b','g','c','m','k','y'};
cancertypes=cancertype.Type;
celltypes=unique(cancertype.Type);
 
 
otherids=setdiff(1:length(ccle.Cancers), cancerids);
pid =1;

for pid = 1:length(pathways.data)

    
    pathwaygenes=pathways.data{pid};
    geneids=arrayfun(@(x) find(strcmp(ccle.Genes, x)), pathwaygenes);
    
    pathwayind = zeros(length(ccle.Genes),1);
    pathwayind(geneids,1)=1;

    ccleview=ccle.data(geneids,:)';
    [pcs, trans, evs] = pca(ccleview); 
   
    

    basis=trans(:,1);
    pc=pcs(:,1);

    numberpositives = length(pc(find(pc > 0)));
     
    numbernegatives = length(pc(find(pc <= 0)));    
     
    if(numbernegatives > numberpositives)
        
        pathwaysign = -1;
    else
        pathwaysign = 1;
    end


    
    cancer_pathway_values(pid, :)= basis*pathwaysign;
    
    %cancer_pathway_values(pid, :)= basis;
    for id=1:size(geneids,1)
    pathwayind(geneids(id), 1)=pc(id);
    end          
    
    genes_pathway_values(pid,:)=pathwayind';
    
    
    
    
fig = figure; 
plot(trans(otherids,1), trans(otherids,2),'.','markersize',20, 'color', [0.5 0.5 0.5]);

hold on
%Change the colors by their cell type
for j = 1:size(cancerids,1)
    color=find(strcmp(celltypes, cancertypes(j)));
    plot(trans(cancerids(j),1), trans(cancerids(j),2), sprintf('%s.',colors{color}),'markersize', 20);
    hold on;
    
    
    
end

xlabel('PC 1')
ylabel('PC 2')
title(sprintf('%s', strrep(pathways.Names{pid}, '_',' ')))



print(fig, '-dpdf', sprintf('Results/plots/PathwayPlots/%s.pdf', pathways.Names{pid}))

close(fig)
end

%Unit normalization

cancer_pathway_values=nanunitnorm(cancer_pathway_values);
cancerpathways.data=cancer_pathway_values';
cancerpathways.Cancers=ccle.Cancers;
cancerpathways.Pathways=pathways.Names;

save('./Processed/cancerpathways.mat','cancerpathways', 'cancer_pathway_values', 'genes_pathway_values')




