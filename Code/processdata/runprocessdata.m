
drugs = {'Erlotinib', 'BIBW2992', 'Gefitinib', 'CL-387785', 'Lapatinib', 'Canertinib', 'PLX4720', 'PD184352', 'SB590885', 'AZD6244', 'Vandetanib', 'Dasatinib', 'Docetaxel'};

names = {'Erlotinib', 'BIBW2992', 'Gefitinib', 'CL-387785', 'Lapatinib', 'Canertinib', 'PLX4720', 'PD184352', 'SB590885', 'AZD6244', 'VEGFR', 'BCRABL', 'Docetaxel'};

EGFR=[{'Erlotinib'}; {'BIBW2992'}; {'Gefitinib'}; {'CL-387785'}];

BRAF=[{'PLX4720'}; {'PD184352'}; {'SB590885'}; {'AZD6244'}];


% Data Process
rescuenormalization=2;
cytokinearray=1;
cytokinenormalization=88;
pathway=20;
cancertype ='all';
pvalthreshold = 1;
dosages_setting='full';

mkdir(sprintf('./Processed/coculturegroupsrescue%dcyto%dnorm%dpathway%d/',  rescuenormalization, cytokinearray, cytokinenormalization, pathway));

for i = 1:size(drugs,2)
processdata(drugs(i), names{i}, rescuenormalization, cytokinearray, cytokinenormalization, pathway, dosages_setting, cancertype);
end

processdata(EGFR, 'EGFR', rescuenormalization, cytokinearray, cytokinenormalization, pathway, dosages_setting, cancertype);
processdata(BRAF, 'BRAF', rescuenormalization, cytokinearray, cytokinenormalization, pathway, dosages_setting, cancertype);




drugnames = {'Erlotinib', 'BIBW2992', 'Gefitinib', 'CL-387785', 'Lapatinib', 'Canertinib', 'PLX4720', 'PD184352', 'SB590885', 'AZD6244', 'VEGFR', 'BCRABL', 'Docetaxel', 'EGFR', 'BRAF'};

for i = 1:size(drugnames, 2)
writerescue(drugnames{i}, rescuenormalization, cytokinearray, cytokinenormalization, pathway);
end

