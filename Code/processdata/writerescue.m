function writerescue(output, rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway)



mkdir(sprintf('./Results/rescue%dcyto%dnorm%dpathway%d/%s/', rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway, output));

input = sprintf('./Processed/coculturegroupsrescue/%s_drug_group.mat',  output);
load(input);

dlmcell(sprintf('./Results/rescue%dcyto%dnorm%dpathway%d/%s/stromas.txt', rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway,output), stromas, '\t')
dlmcell(sprintf('./Results/rescue%dcyto%dnorm%dpathway%d/%s/cancers.txt', rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway,output), cancers, '\t')


%---------------------------------The Rescue Matrix--------------------------------------




for rid = 1:size(Y.rescue, 1)

drug = Y.description{rid}.drug ;
dosage = Y.description{rid}.dosage;
description = {drug dosage 'V3' 'V4' 'V5' 'V6' 'V7' 'V8' 'V9' 'V10' 'V11' 'V12' 'V13' 'V14' 'V15' 'V16'  'V17' 'V18'};    
dlmcell(sprintf('./Results/rescue%dcyto%dnorm%dpathway%d/%s/%s_%2.2f_Raw_Rescue.txt', rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway,output, drug, dosage), description, '\t')
rescue = num2cell(Y.rescue{rid});
dlmcell(sprintf('./Results/rescue%dcyto%dnorm%dpathway%d/%s/%s_%2.2f_Raw_Rescue.txt', rescuenormalization, cytokinearraynumber, cytokinenormalization, pathway,output, drug, dosage), rescue, '\t', '-a')

end   

