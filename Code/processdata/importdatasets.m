

rescuerawdata=importdata('Data/rescue.txt')
rescue.data=rescuerawdata.data;
rescue.textdata=[rescuerawdata.textdata(2:end, 1:4) num2cell(rescuerawdata.data)];
rescue.Cancer_Type=rescuerawdata.textdata(2:end, 1);
rescue.Cancer=rescuerawdata.textdata(2:end, 2);
rescue.Stroma=rescuerawdata.textdata(2:end, 3);
rescue.Drug=rescuerawdata.textdata(2:end, 4);
rescue.uM=rescuerawdata.data(:, 1);
rescue.Effect_Without_Stroma=rescuerawdata.data(:, 2);
rescue.Effect_With_Stroma=rescuerawdata.data(:, 3);
rescue.Rescue=rescuerawdata.data(:, 4);

save('Processed/rescue.mat', 'rescue');


load('Processed/rescue.mat')


cancers=importdata('Data/usedcancer.xls');
stromas=importdata('Data/usedstroma.xls');
drugs=importdata('Data/useddrugs.xls');


drugids = arrayfun(@(x) find(strcmp(rescue.Drug, x)), drugs, 'UniformOutput', false);
drugids =  vertcat(drugids{:});
filteredrescue=subsetrescues(rescue, drugids)

cancersintersect=intersect(cancers, filteredrescue.Cancer);
cancerids = arrayfun(@(x) find(strcmp(filteredrescue.Cancer, x)), cancersintersect, 'UniformOutput', false);
cancerids = vertcat(cancerids{:});
filteredrescue=subsetrescues(filteredrescue, cancerids)

stromasintersect=intersect(stromas, filteredrescue.Stroma);
stromaids = arrayfun(@(x) find(strcmp(filteredrescue.Stroma, x)), stromasintersect, 'UniformOutput', false);
stromaids = vertcat(stromaids{:});
filteredrescue=subsetrescues(filteredrescue, stromaids)

save('Processed/filteredrescue.mat', 'filteredrescue');


colortyperawdata=importdata('Data/colors.xls')
colortype.Type=colortyperawdata.textdata(:,1);
colortype.data=colortyperawdata.data;
save('Processed/colortype.mat', 'colortype')


tissuetyperawdata=importdata('Data/primarysite.xls')
tissuetype.Cancer=tissuetyperawdata.textdata(2:end,1);
tissuetype.Type=tissuetyperawdata.textdata(2:end,2);
tissuetype.Cancer{1,1}='5637';
tissuetype.Cancer{2,1}='697';
save('Processed/tissuetype.mat', 'tissuetype')


cancertyperawdata=importdata('Data/cancertype.xls')
cancertype.Cancer=cancertyperawdata.textdata(:,1);
cancertype.Type=cancertyperawdata.textdata(:,2);
cancertype.Color=cancertyperawdata.textdata(:,3);
cancertype.data=cancertyperawdata.data;
save('Processed/cancertype.mat', 'cancertype')


activedrugsrawdata=importdata('Data/ActiveDrugs.txt');
activedrugs.uM=activedrugsrawdata.data;
activedrugs.Drug=activedrugsrawdata.textdata(2:end,1);
save('Processed/activedrugs.mat', 'activedrugs');


curatedpathways=importdata('Data/pathways.txt')
save('Processed/curatedpathways.mat', 'curatedpathways')
