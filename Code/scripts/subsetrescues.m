function filtered= subsetrescues(rescue,ids)


filtered.data = rescue.data(ids, :);
filtered.textdata = rescue.textdata(ids, :);

%From the text data
filtered.Cancer_Type = rescue.Cancer_Type(ids);
filtered.Cancer = rescue.Cancer(ids);
filtered.Stroma = rescue.Stroma(ids) ;
filtered.Drug = rescue.Drug(ids);
%From the numerical data
filtered.uM = rescue.uM(ids);
filtered.Effect_Without_Stroma = rescue.Effect_Without_Stroma(ids);
filtered.Effect_With_Stroma = rescue.Effect_With_Stroma(ids);
filtered.Rescue = rescue.Rescue(ids);
%filtered.logRescue = rescue.logRescue(ids);
end