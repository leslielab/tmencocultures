

function matrix = nanunitnorm(X)
matrix=zeros(size(X,1), size(X, 2));
for (i =1:size(X,2))
    col=X(:,i);
    
    nancol = col(~isnan(col));
    squarenorm=sqrt(sum(nancol.^2));

    col = col/squarenorm;
    
    matrix(:,i)=col;
end
end
