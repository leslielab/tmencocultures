
path = '/cbio/cllab/home/jly/projects/tmen/Project';
addpath(genpath([sprintf('%s/code',path)]));


input = sprintf('%s/Results/plots/ARMTLvsLeastRPerformance/ARMTLvsLeastRPerformance.mat',  path);

load(input);


drugs = {'Erlotinib', 'BIBW2992', 'Gefitinib', 'CL-387785', 'Lapatinib', 'Canertinib', 'PLX4720', 'PD184352', 'SB590885', 'AZD6244', 'VEGFR', 'BCRABL', 'Docetaxel', 'EGFR', 'BRAF'};

lambdas = ParametersARMTL(:,1);
rhos = ParametersARMTL(:,2);


%The Settings for AR
rho=0;
rescuenormalization=2;
cytokinearray=1;
cytokinenormalization=88;
pathway=20;
permutations=10000;


%Run Affinity Regression for individual and group drugs
%Drugs individual
for i = [1:15]
generateempiricalpvalues(drugs{i},  rescuenormalization, cytokinearray, cytokinenormalization, pathway, lambdas(i), rho, rhos(i), permutations, path);
end
