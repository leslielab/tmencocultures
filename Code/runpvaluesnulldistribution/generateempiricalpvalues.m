function generateempiricalpvalues(drug_group,  rescuenormalization, cytokinearray, cytokinenormalization, pathwaynumber, lambda, rho1, rho2, permutations, path)


datasource = 'TMEN';
fprintf('Running %s : %s\n', datasource, drug_group);
inputdataset = sprintf('./Processed/coculturegroupsrescue%dcyto%dnorm%dpathway%d/%s_drug_group.mat',  rescuenormalization, cytokinearray, cytokinenormalization, pathwaynumber, drug_group);
load(inputdataset);

inputmodel=sprintf('models/Pathway_WholeModels_JointGroup/%s/models_rescue%d_cyto%d_norm%d_pathway%d_wholemodel.mat', drug_group,  rescuenormalization, cytokinearray, cytokinenormalization, pathwaynumber);
load(inputmodel);

C = model.C.data;
S = model.S.data';

W=reshape(model.Wcommon,[size(C,2) size(S,2)]);
CCM = C*reshape(model.Wcommon,[size(C,2) size(S,2)]);
PSM = reshape(model.Wcommon, [size(C,2) size(S,2)])*S';

N = permutations;
W_perm = cell(N ,1);
CCM_perm = cell(N ,1);
PSM_perm = cell(N ,1);
for p = 1:N
        X=kron(S,C);
        number_tasks=size(Y.rescue,1);
        data.Y=cell(1,number_tasks);
        data.X=cell(1,number_tasks);
        data.info=cell(1,number_tasks);
        
for t = 1:number_tasks      
        %convert Y into a vector
        rng('shuffle'); 
        Y_perm=Y.rescue{t}(randperm(size(Y.rescue{t}, 1)), randperm(size(Y.rescue{t}, 2)));
        Y_t=Y_perm(:);
        %Get rid of the nans
        idx=find(~isnan(Y_t));
        Y_t=Y_t(idx);
        data.Y{t} = Y_t;
        %Get the corresponding rows of the X 
        X_t=X(idx,:);
        data.X{t} = X_t;
        %information drug and dosage
        data.info{t}=Y.description{t};
end
        
    
t=number_tasks;
R = eye(t) - ones(t)/t;

rng('default');     % reset random generator.Available from Matlab 2011.
opts.init = 0;      % guess start point from data. 
opts.tFlag = 1;     % terminate after relative objective value does not changes much.
opts.tol = 10^-5;   % tolerance. 
opts.maxIter = 500; % maximum iteration number of optimization.

opts.rho_L2=rho2;

display(sprintf('Running TMEN: %s Permutation: %g', drug_group, p))

[W_tasks funcVal] = Least_SRMTL(data.X, data.Y, R, lambda, rho1, opts);

W_common=(lambda/(rho2+lambda))*(1/t)*mean(W_tasks,2);
    
    W_perm{p} = reshape(W_common, [size(C,2) size(S,2)]);
    CCM_perm{p} = C*W_perm{p};
    PSM_perm{p} = W_perm{p}*S';
 
end
 
my_psm_left = zeros(size(C,2),size(S,1));
my_psm_right = zeros(size(C,2),size(S,1));
for p = 1:N
    for pathway = 1:size(C,2)
        for stroma = 1:size(S,1)
            if(PSM_perm{p}(pathway,stroma)>PSM(pathway,stroma))
                my_psm_left(pathway,stroma) = my_psm_left(pathway,stroma) + 1;
            end;
            if(PSM_perm{p}(pathway,stroma)<PSM(pathway,stroma))
                my_psm_right(pathway,stroma) = my_psm_right(pathway,stroma) + 1;
            end;
        end
    end
end

fname_left = sprintf('%s/Results/Pathways/rescue%dcyto%dnorm%dpathway%d/%s/my_psm_left_10000.csv', path, rescuenormalization, cytokinearray, cytokinenormalization, pathwaynumber, drug_group);
fname_right = sprintf('%s/Results/Pathways/rescue%dcyto%dnorm%dpathway%d/%s/my_psm_right_10000.csv',  path, rescuenormalization, cytokinearray, cytokinenormalization, pathwaynumber, drug_group);
xlswrite(fname_left, my_psm_left);
xlswrite(fname_right, my_psm_right);



my_ccm_left = zeros(size(C,1), size(S,2));
my_ccm_right = zeros(size(C,1), size(S,2));
for p = 1:N
    for cytokine= 1:size(S,2)
        for cancer = 1:size(C,1)
            if(CCM_perm{p}(cancer,cytokine)>CCM(cancer,cytokine))
                my_ccm_left(cancer,cytokine) = my_ccm_left(cancer,cytokine) + 1;
            end;
            if(CCM_perm{p}(cancer,cytokine)<CCM(cancer,cytokine))
                my_ccm_right(cancer, cytokine) = my_ccm_right(cancer, cytokine) + 1;
            end;
        end
    end
end

my_ccm_left=num2cell(my_ccm_left);
my_ccm_right=num2cell(my_ccm_right);
 
fname_left = sprintf('%s/Results/Pathways/rescue%dcyto%dnorm%dpathway%d/%s/my_ccm_left_10000.csv', path, rescuenormalization, cytokinearray, cytokinenormalization, pathwaynumber, drug_group);
fname_right = sprintf('%s/Results/Pathways/rescue%dcyto%dnorm%dpathway%d/%s/my_ccm_right_10000.csv',  path, rescuenormalization, cytokinearray, cytokinenormalization, pathwaynumber, drug_group);
xlswrite(fname_left, my_ccm_left);
xlswrite(fname_right, my_ccm_right);



my_w_left = zeros(size(C,2), size(S,2));
my_w_right = zeros(size(C,2), size(S,2));
for p = 1:N
	for pathway = 1:size(C,2)
        for cytokine= 1:size(S,2)
            if(W_perm{p}(pathway,cytokine)>W(pathway,cytokine))
                my_w_left(pathway,cytokine) = my_w_left(pathway,cytokine) + 1;
            end;
            if(W_perm{p}(pathway,cytokine)<W(pathway,cytokine))
                my_w_right(pathway, cytokine) = my_w_right(pathway, cytokine) + 1;
            end;
        end
    end
end
my_w_left=num2cell(my_w_left);
my_w_right=num2cell(my_w_right);
 
fname_left = sprintf('%s/Results/Pathways/rescue%dcyto%dnorm%dpathway%d/%s/my_w_left_10000.csv', path, rescuenormalization, cytokinearray, cytokinenormalization, pathwaynumber, drug_group);
fname_right = sprintf('%s/Results/Pathways/rescue%dcyto%dnorm%dpathway%d/%s/my_w_right_10000.csv',  path, rescuenormalization, cytokinearray, cytokinenormalization, pathwaynumber, drug_group);
xlswrite(fname_left, my_w_left);
xlswrite(fname_right, my_w_right);


end